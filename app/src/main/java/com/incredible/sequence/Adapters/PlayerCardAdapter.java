package com.incredible.sequence.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.incredible.sequence.Objects.Card;
import com.incredible.sequence.R;
import com.incredible.sequence.Utils.RecyclerViewClickListener;

import java.util.ArrayList;

/**
 * Created by Dakshal on 11/29/2017.
 */

public class PlayerCardAdapter extends RecyclerView.Adapter<PlayerCardAdapter.ViewHolder> {

    private Context context;
    ArrayList<Card> cards;
    private static RecyclerViewClickListener itemListener;

    public PlayerCardAdapter(Context context, RecyclerViewClickListener itemListener, ArrayList<Card> cards) {
        this.cards = cards;
        this.context = context;
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String cardName = cards.get(position).getType().toLowerCase().charAt(0) + cards.get(position).getValue().toLowerCase();
        holder.ivCard.setImageResource(getImageId(context, cardName));
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 9;


        holder.ivBorder.getLayoutParams().width = devicewidth;
        holder.ivBorder.getLayoutParams().height = (int) (devicewidth*1.417);

        holder.ivCard.getLayoutParams().width = devicewidth;
        holder.ivCard.getLayoutParams().height = (int) (devicewidth*1.417);

        if(cards.get(position).isBordered()){
            holder.ivBorder.setBackgroundResource(R.drawable.border_selcted);
        } else {
            holder.ivBorder.setBackgroundResource(R.drawable.border);
        }
//        Picasso.with(context).load(getImageId(context, cardName)).fit().centerInside().into(holder.ivCard);

    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView ivCard;
        ImageView ivBorder;

        public ViewHolder(View view) {
            super(view);
            ivCard = view.findViewById(R.id.ivCard);
            ivBorder = view.findViewById(R.id.ivBorder);
            ivCard.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

    public void updateData(ArrayList<Card> cards) {
        this.cards = cards;
        notifyDataSetChanged();
    }
}

