package com.incredible.sequence.Adapters;

/**
 * Created by Dakshal on 12/13/2017.
 */

public interface TaskCompletion {
    public void taskCompletionResult(boolean result);
}