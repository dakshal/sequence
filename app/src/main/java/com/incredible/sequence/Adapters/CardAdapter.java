package com.incredible.sequence.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.incredible.sequence.Objects.BoardCard;
import com.incredible.sequence.Objects.Card;
import com.incredible.sequence.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dakshal on 11/28/2017.
 */

public class CardAdapter extends BaseAdapter {
    private Context context;
    private BoardCard[][] boardCards;
    private String TAG = "CardAdapter";

    public CardAdapter(Context context, BoardCard[][] boardCards) {
        this.context = context;
        this.boardCards = boardCards;
    }

    public void updateData(BoardCard[][] boardCards) {
        this.boardCards = boardCards;
        notifyDataSetChanged();
    }


    public View getView(int position, View view, ViewGroup parent) {

//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;
        int x = position / 10;
        int y = position % 10;
        Card card = boardCards[x][y].getCard();

//        Log.d(TAG, "getView: "+position+"\t"+position/10+"\t"+position%10);

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

//        Log.i(TAG, "getView: "+card.getType()+"\t"+ card.getValue()+"\t"+position);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 10;

        //if you need 4-5-6 anything fix imageview in height

//        holder.rlItems.getLayoutParams().width = devicewidth;
//        holder.rlItems.getLayoutParams().height = deviceheight;
//
//        //if you need same height as width you can set devicewidth in holder.image_view.getLayoutParams().height
//        holder.rlItems.getLayoutParams().height = deviceheight;
        if (boardCards[x][y].getCard() == null) {
//            Bitmap bmp1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.jack);
//            Bitmap newBmp = Bitmap.createScaledBitmap(R.drawable.jack, devicewidth, (int)(devicewidth*1.42f), false);
            holder.ivCard.setImageResource(R.drawable.jack);
        } else {
            String cardName = card.getType().toLowerCase().charAt(0) + card.getValue().toLowerCase();
//            Log.i(TAG, "getView: " + cardName);
//            Picasso.with(context).load(getImageId(context, cardName)).fit().centerInside().into(holder.ivCard);
//            Bitmap bmp1 = BitmapFactory.decodeResource(context.getResources(), getImageId(context, cardName));
            holder.ivCard.setImageResource(getImageId(context, cardName));
//            holder.rlItems.setBackgroundResource(getImageId(context, cardName));
//            holder.ivCard.setImageResource(context.getResources().getIdentifier("R.drawable."+card.getType().toLowerCase().charAt(0)+card.getValue().toLowerCase(), "drawable", context.getPackageName()));
        }

        holder.ivBorder.getLayoutParams().width = devicewidth;
        holder.ivBorder.getLayoutParams().height = (int) (devicewidth*1.417);
        holder.ivFilled.getLayoutParams().width = devicewidth;
        holder.ivFilled.getLayoutParams().height = (int) (devicewidth*1.417);
        holder.ivCoin.getLayoutParams().width = devicewidth;
        holder.ivCoin.getLayoutParams().height = (int) (devicewidth*1.417);

        if(boardCards[x][y].getCoin()!=null){
            if(boardCards[x][y].getCoin().getColor() == 2){
                holder.ivCoin.setImageResource(R.drawable.coin_blue);
                holder.ivCoin.setVisibility(View.VISIBLE);
            } else if(boardCards[x][y].getCoin().getColor() == 4){
                holder.ivCoin.setImageResource(R.drawable.coin_green);
                holder.ivCoin.setVisibility(View.VISIBLE);
            } else if(boardCards[x][y].getCoin().getColor() == 6){
                holder.ivCoin.setImageResource(R.drawable.coin_red);
                holder.ivCoin.setVisibility(View.VISIBLE);
            }
        } else {
            holder.ivCoin.setVisibility(View.GONE);
        }

        if(boardCards[x][y].isFilled()){
            if(boardCards[x][y].getCoin().getColor() == 2) {
                holder.ivFilled.setBackgroundResource(R.drawable.border_filled_blue);
                holder.ivFilled.setVisibility(View.VISIBLE);
            } else if(boardCards[x][y].getCoin().getColor() == 4){
                holder.ivFilled.setBackgroundResource(R.drawable.border_filled_green);
                holder.ivFilled.setVisibility(View.VISIBLE);
            } else {
                holder.ivFilled.setBackgroundResource(R.drawable.border_filled_red);
                holder.ivFilled.setVisibility(View.VISIBLE);
            }
        } else {
//            Log.d(TAG, "getView: isSelected:- "+boardCards[x][y].isSelected()+"\tx:- "+x+"\ty:- "+y);
            if(boardCards[x][y].isSelected()){
                Log.d(TAG, "getView: isSelected:- "+boardCards[x][y].isSelected()+"\tx:- "+x+"\ty:- "+y);
                holder.ivFilled.setBackgroundResource(R.drawable.border_selcted);
                holder.ivFilled.setVisibility(View.VISIBLE);
            } else {
                holder.ivFilled.setBackgroundResource(R.drawable.border);
            }
        }

        return view;
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    @Override
    public int getCount() {
        return boardCards.length * boardCards[0].length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {
        @BindView(R.id.ivCard)
        ImageView ivCard;

        @BindView(R.id.ivCoin)
        ImageView ivCoin;

        @BindView(R.id.ivFilled)
        ImageView ivFilled;

        @BindView(R.id.ivBorder)
        ImageView ivBorder;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}