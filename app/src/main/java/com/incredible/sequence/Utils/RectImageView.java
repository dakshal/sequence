package com.incredible.sequence.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatImageView;
import android.widget.LinearLayout;

/**
 * Created by Dakshal on 11/30/2017.
 */

public class RectImageView extends LinearLayout {
    public RectImageView(Context context) {
        super(context);
    }

    public RectImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RectImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
}
