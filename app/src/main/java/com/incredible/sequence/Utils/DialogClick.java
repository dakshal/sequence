package com.incredible.sequence.Utils;

import android.view.View;

import com.incredible.sequence.R;

/**
 * Created by Dakshal on 30/11/17.
 */
public abstract class DialogClick implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAllow:
                //write action for on press of btnAccept
                btnClickYes();
                break;
            case R.id.btnSkip:
                //write action for on press of btnCancel
                btnClickNo();
                break;

        }
    }

   abstract public void btnClickYes();

   abstract public void btnClickNo();



}
