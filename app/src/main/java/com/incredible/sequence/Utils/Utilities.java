package com.incredible.sequence.Utils;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.incredible.sequence.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Dakshal on 30/11/17.
 */

public class Utilities {

    private static final String TAG = "Utilities";
    public static String rupeeSymbol = "\u20B9";
    public static ArrayList<Integer> diagnoseFlow = new ArrayList<>(Collections.nCopies(7, -1));//Collections.nCopies(5,-1)

    private ObjectAnimator mProgressBarAnimator;
    public static boolean isCustomDiagnosis = false;

    public static Utilities instance = new Utilities();
    private ProgressDialog progressDialog;
    public Dialog dialogConfirmation;
//    private Dialog loadingDialog;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private Utilities() {

    }

    public int dpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public void alert(Context context, String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.app_name))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setMessage(message).show();

    }

    public Bitmap returnBitmap(Context context, int image) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), image);
        return bitmap;
    }

    public String millisToHMS(int millis) {
        String timeInHMS = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));


        return timeInHMS;
    }


//    public void progressBarAnimate(final HoloCircularProgressBar progressBar, final Animator.AnimatorListener listener,
//                                   final float progress, final int duration) {
//
//        mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress", progress);
//        mProgressBarAnimator.setDuration(duration);
//
//        mProgressBarAnimator.addListener(new Animator.AnimatorListener() {
//
//            @Override
//            public void onAnimationCancel(final Animator animation) {
//                progressBar.setProgress(progress);
//            }
//
//            @Override
//            public void onAnimationEnd(final Animator animation) {
//                progressBar.setProgress(progress);
//            }
//
//            @Override
//            public void onAnimationRepeat(final Animator animation) {
//            }
//
//            @Override
//            public void onAnimationStart(final Animator animation) {
//            }
//        });
//        if (listener != null) {
//            mProgressBarAnimator.addListener(listener);
//        }
//        mProgressBarAnimator.reverse();
//        mProgressBarAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//
//            @Override
//            public void onAnimationUpdate(final ValueAnimator animation) {
//                progressBar.setProgress((Float) animation.getAnimatedValue());
//            }
//        });
//        mProgressBarAnimator.start();
//    }

    public void hapticFeedback(Context context, int time) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(time);
    }

    public void showProgress(String message, Context context) {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(context.getString(R.string.app_name));
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void dismissProgress() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();

        }

    }

    public String getFormattedLocalDate(String time) {

        String inputPattern = "yyyy-MM-dd HH:mm:ss";

//        Log.d("Time Server ", time);

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat outputFormat = new SimpleDateFormat(inputPattern, Locale.US);

        Date date = new Date();
        String str = null;

        if (time == null) {

            return "";
        }

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);

            //Log.v("parseDate", "Converted Date Today:" + str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

    public void shortSnack(View view, String message) {

        if (view != null) {

            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
            snackbar.show();

            View snackView = snackbar.getView();

            TextView tv = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        }
    }

    public void longSnack(View view, String message) {

        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.show();

        View snackView = snackbar.getView();

        TextView tv = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {
            isValid = true;
        }

        return isValid;
    }



    public String getCurrentTimeHHmmss() {

        SimpleDateFormat strToDate = new SimpleDateFormat("HH:mm:ss");

        return strToDate.format(new Date());
    }

    public String getCurrentDate() {

        SimpleDateFormat strToDate = new SimpleDateFormat("yyyy-MM-dd");

        return strToDate.format(new Date());
    }

    public String getCurrentDateTime() {

        SimpleDateFormat strToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return strToDate.format(new Date());
    }

    public String getFormattedTime(String strDate) {

        final SimpleDateFormat inputSDF = new SimpleDateFormat("HH:mm:ss");
        inputSDF.setTimeZone(TimeZone.getDefault());

        try {

            Date date = inputSDF.parse(strDate);

            System.out.println("Input time: " + date);

            final SimpleDateFormat outputSDF = new SimpleDateFormat("HH:mm:ss");
            outputSDF.setTimeZone(TimeZone.getTimeZone("UTC"));

            String output = outputSDF.format(date);

            System.out.println("UTC time: " + output);

            return output;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getFormattedDate(String time, String outputPattern) {

        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        if (time == null) {

            return "";
        }

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);

            //Log.v("parseDate", "Converted Date Today:" + str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

    public String getFormattedDate(String time, String inputPattern, String outputPattern) {

        // String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        if (time == null) {

            return "";
        }

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);

            //Log.v("parseDate", "Converted Date Today:" + str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

    public String getScheduleDateTime(String strDate, String time) {

        final SimpleDateFormat inputSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        inputSDF.setTimeZone(TimeZone.getDefault());

        try {

            Date date = inputSDF.parse(strDate);

            System.out.println("Input Date Time: " + date);

            final SimpleDateFormat outputSDF = new SimpleDateFormat("yyyy-MM-dd");
            outputSDF.setTimeZone(TimeZone.getTimeZone("UTC"));

            String output = outputSDF.format(date);

            System.out.println("UTC Date Time: " + output);

            return output + " " + time;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getSlotScheduleDateTime(String strDate, String time) {

        final SimpleDateFormat inputSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        try {

            Date date = inputSDF.parse(strDate);

            System.out.println("Input Date Time: " + date);

            final SimpleDateFormat outputSDF = new SimpleDateFormat("yyyy-MM-dd");

            String output = outputSDF.format(date);

            return output + " " + time;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getSlotTime(String inputTime) {

        final SimpleDateFormat inputSDF = new SimpleDateFormat("HH:mm:ss");
        inputSDF.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {

            Date date = inputSDF.parse(inputTime);

            System.out.println("Input time: " + date);

            final SimpleDateFormat outputSDF = new SimpleDateFormat("hh.mm aa");
            outputSDF.setTimeZone(TimeZone.getDefault());

            String output = outputSDF.format(date);

            System.out.println("UTC time: " + output);

            return output;

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return "";
    }

    /**
     * @param context
     * @param imageDrawable   pass 0 to hide image
     * @param onClickListener
     * @param stringContent   To Change the title ,content & button name pass String parameters accordingly
     * @see //In In the 4th parameter(
     * <br>
     * -To Change the title ,content & button name pass String parameters accordingly<br>
     * {1}param 1 -Title<br>
     * {2}param 2 -Content<br>
     * {3}param 3 -Button 1<br>
     * {4}param 4 -Button 2 )
     */

    public void showDialog(final Context context, int imageDrawable, DialogClick onClickListener, String... stringContent) {


        dialogConfirmation = new Dialog(context, R.style.WhiteBGDialog);
        dialogConfirmation.setContentView(R.layout.dialog_confirmation_item);
        TextView tvDialogContent = (TextView) dialogConfirmation.findViewById(R.id.tvDialogContent);
        TextView tvDialogTitle = (TextView) dialogConfirmation.findViewById(R.id.tvDialogTitle);
        ImageView ivDialogIcon = (ImageView) dialogConfirmation.findViewById(R.id.ivDialogIcon);
        Button btnCommon = (Button) dialogConfirmation.findViewById(R.id.btnAllow);
        Button btnSkip = (Button) dialogConfirmation.findViewById(R.id.btnSkip);

        if (imageDrawable == 0) {
            ivDialogIcon.setVisibility(View.GONE);
        } else {
            ivDialogIcon.setImageResource(imageDrawable);
        }
        btnSkip.setVisibility(View.VISIBLE);

        dialogConfirmation.setCancelable(false);

        try {
            tvDialogTitle.setText(Html.fromHtml(stringContent[0]));
        } catch (ArrayIndexOutOfBoundsException e) {
            btnCommon.setOnClickListener(onClickListener);
            btnSkip.setOnClickListener(onClickListener);
            dialogConfirmation.show();

            return;
        }
        try {
            tvDialogContent.setText(Html.fromHtml(stringContent[1]));
        } catch (ArrayIndexOutOfBoundsException e) {
            btnCommon.setOnClickListener(onClickListener);
            btnSkip.setOnClickListener(onClickListener);
            dialogConfirmation.show();

            return;

        }
        try {
            btnCommon.setText(stringContent[2]);
        } catch (ArrayIndexOutOfBoundsException e) {
            btnCommon.setOnClickListener(onClickListener);
            btnSkip.setOnClickListener(onClickListener);
            dialogConfirmation.show();

            return;
        }
        try {
            btnSkip.setText(stringContent[3]);
            btnSkip.setVisibility(View.VISIBLE);
        } catch (ArrayIndexOutOfBoundsException e) {
            btnSkip.setVisibility(View.GONE);

        }


        btnCommon.setOnClickListener(onClickListener);
        btnSkip.setOnClickListener(onClickListener);
        dialogConfirmation.show();

    }


    public void dismissDialog() {

        if (dialogConfirmation != null && dialogConfirmation.isShowing()) {
            dialogConfirmation.dismiss();
        }
    }

    public String colorThisText(String yourText, int yourColor, Context context) {

        String input = yourText;
        Resources res = context.getResources();
//        String coloredOutput="<font color='#"+res.getString(yourColor)+"'>"+input+"</font>";
        String coloredOutput = "<font color='" + '#' + Integer.toHexString(res.getColor(yourColor) & 0x00ffffff) + "'>" + input + "</font>";
        return coloredOutput;

    }

//    public void showLoadingDialog(Context context, String message, boolean isCancellable) {
//
//        loadingDialog = new Dialog(context, R.style.TransparentDialog);
//        loadingDialog.setContentView(R.layout.loading_dialog);
//        loadingDialog.setCanceledOnTouchOutside(false);
//
//        if (isCancellable) {
//            loadingDialog.setCancelable(true);
//        } else {
//            loadingDialog.setCancelable(false);
//        }
//
//        TextView tvLoadingText = (TextView) loadingDialog.findViewById(R.id.tvLoadingText);
//
//        if (message != null && !message.isEmpty()) {
//            tvLoadingText.setVisibility(View.VISIBLE);
//            tvLoadingText.setText(message);
//        } else {
//            tvLoadingText.setVisibility(View.GONE);
//        }
//
//        loadingDialog.show();
//    }
//
//    public void dismissLoadingDialog() {
//
//        if (loadingDialog != null && loadingDialog.isShowing()) {
//            loadingDialog.dismiss();
//        }
//    }

    public void openLinkInBrowser(Context context, String url) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

    public static String getTimeAgo(Context context, long time) {

        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();
        if (time > now || time <= 0) {
            return null;
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return context.getResources().getString(R.string.just_now);
        } else if (diff < 2 * MINUTE_MILLIS) {
            return context.getResources().getString(R.string.minute_ago);
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " " + context.getResources().getString(R.string.minutes_ago);
        } else if (diff < 120 * MINUTE_MILLIS) { // 90
            return context.getResources().getString(R.string.hour_ago);
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " " + context.getResources().getString(R.string.hours_ago);
        } else if (diff < 48 * HOUR_MILLIS) {
            return context.getResources().getString(R.string.yesterday);
        } else {
            return diff / DAY_MILLIS + " " + context.getResources().getString(R.string.days_ago);
        }
    }

}
