package com.incredible.sequence.Utils;

import android.view.View;

/**
 * Created by Dakshal on 11/30/2017.
 */

public interface RecyclerViewClickListener
{
    public void recyclerViewListClicked(View v, int position);

}