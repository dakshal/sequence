package com.incredible.sequence;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.incredible.sequence.Adapters.CardAdapter;
import com.incredible.sequence.Adapters.PlayerCardAdapter;
import com.incredible.sequence.Adapters.TaskCompletion;
import com.incredible.sequence.Objects.AI.AICard;
import com.incredible.sequence.Objects.AI.AIPlayer;
import com.incredible.sequence.Objects.AI.UpgradedAIPlayer;
import com.incredible.sequence.Objects.Board;
import com.incredible.sequence.Objects.BoardCard;
import com.incredible.sequence.Objects.Card;
import com.incredible.sequence.Objects.Coin;
import com.incredible.sequence.Objects.Deck;
import com.incredible.sequence.Objects.Player;
import com.incredible.sequence.Objects.Status;
import com.incredible.sequence.Utils.AbstractBaseActivity;
import com.incredible.sequence.Utils.DialogClick;
import com.incredible.sequence.Utils.Utilities;
import com.incredible.sequence.Utils.RecyclerViewClickListener;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
//import com.orhanobut.logger.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChooseType extends AbstractBaseActivity implements RecyclerViewClickListener {

    @BindView(R.id.gvBoard)
    GridView gvBoard;

    @BindView(R.id.lvPlayerCards)
    RecyclerView lvPlayerCards;

    @BindView(R.id.rlGameBoard)
    LinearLayout rlGameBoard;

    @BindView(R.id.tvReplace)
    TextView tvReplace;

    @BindView(R.id.ivRecentlyUsedCard)
    ImageView ivRecentlyUsedCard;


    private CardAdapter cardAdapter;
    BoardCard[][] game;
    int turn = 0;       // EVEN = player1 ODD = computer/player2
    Board board;
    Deck deck;
    Status status;
    Card selectedCard;
    private static final int NO_OF_TEAMS = 2;
    private static final int NO_OF_PLAYERS = 2;
    private ArrayList<Player> players = new ArrayList<>();
    private String TAG = "ChooseType";
    PlayerCardAdapter playerCardAdapter;
    HashMap<Integer, ArrayList<Integer>> cardMapping = new HashMap<>();
    ArrayList<Integer> computerPositions = new ArrayList<>();
    ArrayList<Integer> opponentPositions = new ArrayList<>();
    ArrayList<Integer> availablePositions = new ArrayList<>();
    //    PlayerCardListAdapter playerCardListAdapter;
    int counter1 = 0, counter2 = 25;
    private int optionSize = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_choose_type);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_IMMERSIVE);
//        } else {
////            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
////                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }

        Bundle b = getIntent().getExtras();
        final boolean isBasic = b.getBoolean(AppConstant.IS_BASIC);
        ButterKnife.bind(this);

        setupGame();
//        verifyPositions();
        gvBoard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int x = position / 10;
                int y = position % 10;

                if (position == 0 || position == 9 || position == 90 || position == 99) {
                    Utilities.instance.longSnack(rlGameBoard, "You can put a coin on Jack");
                } else {
                    if (availablePositions.isEmpty() || availablePositions.size() == 0 || availablePositions == null) {
                        Utilities.instance.shortSnack(rlGameBoard, "Select a card first");
                        return;
                    }
                    if (availablePositions.contains(position) || (cardMapping.get(selectedCard.getNumber()).contains(100) && game[x][y].getCoin() == null) || (cardMapping.get(selectedCard.getNumber()).contains(-1) && game[x][y].getCoin().getColor() == players.get((turn + 1) % NO_OF_PLAYERS).getCoin().getColor())) {
                        Card card = (Card) cardAdapter.getItem(position);
                        Player player = players.get(turn % NO_OF_PLAYERS);
//                Log.d(TAG, "onItemClick: " + turn + "\t" + player.getCards().get(0).getType() + "\t" + player.getCards().get(0).getValue());

//                    Card selectedCard = player.getCards().get(position);

                        if (setNextCard(player, x, y)) {
                            opponentPositions.add(x * 10 + y);
                            checkGameBoard(player);
                            if (availablePositions.size() != 0) {
                                int i = 0;
                                while (i < availablePositions.size()) {
                                    int pos = availablePositions.get(i);
                                    game[pos / 10][pos % 10].setSelected(false);
                                    i++;
                                }
                                availablePositions.clear();
                            }
                            String cardName = selectedCard.getType().toLowerCase().charAt(0) + selectedCard.getValue().toLowerCase();
                            ivRecentlyUsedCard.setImageResource(getImageId(context, cardName));

//                            Log.d(TAG, "onItemClick: turn:- "+turn);
                            if (isBasic) {
                                cardAdapter.updateData(game);
                                playerCardAdapter.updateData(players.get((turn) % NO_OF_PLAYERS).getCards());
                                Utilities.instance.shortSnack(rlGameBoard, "Player " + (((turn) % NO_OF_PLAYERS) + 1) + "'s Turn");
                            } else {
                                cardAdapter.updateData(game);
                                new ThinkInProgress().execute(new TaskCompletion() {
                                    @Override
                                    public void taskCompletionResult(boolean result) {
                                        checkGameBoard(players.get((turn + 1) % NO_OF_PLAYERS));
                                        playerCardAdapter.updateData(players.get((turn) % NO_OF_PLAYERS).getCards());
                                        cardAdapter.updateData(game);
                                        String cardName = cardToReplace.getType().toLowerCase().charAt(0) + cardToReplace.getValue().toLowerCase();
                                        ivRecentlyUsedCard.setImageResource(getImageId(context, cardName));
                                    }
                                });
//                                Log.d(TAG, "onItemClick: turn:- "+turn);
                            }
                        } else {
                            Utilities.instance.shortSnack(rlGameBoard, "Invalid Position");
                        }

                    } else {
                        Utilities.instance.shortSnack(rlGameBoard, "You can only select highlighted positions");
                    }
                }
            }
        });
    }

    private class ThinkInProgress extends AsyncTask<TaskCompletion, Void, String> {
        private TaskCompletion delegate;

        @Override
        protected String doInBackground(TaskCompletion... params) {
            try {
                Thread.sleep(1);
                takeAIturn(players.get((turn) % NO_OF_PLAYERS), players.get((turn + 1) % NO_OF_PLAYERS));
                this.delegate = params[0];
//                delegate.taskCompletionResult(true);
//                cardAdapter.updateData(game);
            } catch (InterruptedException e) {
                Thread.interrupted();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Utilities.instance.dismissProgress();
            delegate.taskCompletionResult(true);
        }

        @Override
        protected void onPreExecute() {
            Utilities.instance.showProgress("Thinking Next Move", context);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void verifyPositions() {
        boolean isMatch = true;
        Board board = new Board();
        HashMap<Integer, ArrayList<Integer>> map = cardMapping;
        BoardCard[][] game = board.getGame();

        for (int i = 0; i < 100; i++) {
            if (game[i / 10][i % 10].getCard() == null) {
                Log.d(TAG, "verifyPositions: Its a jack");
                continue;
            }
//            Log.d(TAG, "verifyPositions: "+game[i/10][i%10].getCard().getNumber()+"\t"+i);
            if (!map.get(game[i / 10][i % 10].getCard().getNumber()).contains(i)) {
                Log.d(TAG, gson.toJson(map.get(game[i / 10][i % 10].getCard().getNumber())));
                Log.d(TAG, "verifyPositions: i = " + i);
            }
        }


    }

    private void checkGameBoard(Player player) {
        player = status.isGameOver(game, player);
//                            Log.d(TAG, "onItemClick: after:- "+gson.toJson(player));
//        Log.d(TAG, "onItemClick: " + status);

//                            players.set(((turn - 1) % NO_OF_PLAYERS), player);
        checkBorders(player.getSequences());

        if (player.getNoOfSequences() < 2) {
//                            Toast.makeText(context, "Player " + (((turn) % NO_OF_PLAYERS) + 1) + "'s Turn", Toast.LENGTH_SHORT).show();
        } else {
            Utilities.instance.showDialog(context, 0, new DialogClick() {
                @Override
                public void btnClickYes() {
                    setupGame();
                    Utilities.instance.dismissDialog();
//                                        Bundle b = new Bundle();
//                                        b.putBoolean(AppConstant.IS_BASIC, true);
//                                        Intent i = new Intent(ChooseType.this, ChooseType.class);
//                                        i.putExtras(b);
//                                        startActivity(i);
//                                        finish();
                }

                @Override
                public void btnClickNo() {
                    setupGame();
                    finish();
                    Utilities.instance.dismissDialog();
                }
            }, "Team " + (((turn - 1) % NO_OF_TEAMS) + 1) + " wins", "Do you want to play again?");
        }
    }

    Card cardToReplace = null;
    int replaceCardPos = -1;


    private boolean takeAIturn(Player player, Player opponent) {
//        turn++;
        int depth = 4;
//        if (turn > 10) {
//            depth = 6;
//        } else if (turn > 20) {
//            depth = 8;
//        } else if (turn > 30) {
//            depth = 10;
//        } else if (turn > 40) {
//            depth = 12;
//        }
        UpgradedAIPlayer aiPlayer = new UpgradedAIPlayer(new Board(game), depth, optionSize);
//        AICard aiCard = aiPlayer.randomCardSelector(player);
        AICard aiCard = aiPlayer.nextMove(player, opponent, computerPositions, opponentPositions, context);
//        Log.d(TAG, "takeAIturn: selected Card:- "+gson.toJson(aiPlayer.nextStep()));
        Log.d(TAG, "takeAIturn: " + gson.toJson(aiCard));
        cardToReplace = aiCard.getCard();
        selectedCard = cardToReplace;
        for (int i = 0; i < player.getCards().size(); i++) {
            if (cardToReplace == player.getCards().get(i)) {
                replaceCardPos = i;
                break;
            }
        }
        if (setNextCard(player, aiCard.getX(), aiCard.getY())) {
            computerPositions.add(aiCard.getX() * 10 + aiCard.getY());
//            updatePlayerCards(player, cardToReplace);
            return true;
        } else {
            return false;
        }
    }


    @OnClick(R.id.tvReplace)
    void replaceUsedCard() {
        if (cardToReplace == null || replaceCardPos == -1) {
            return;
        }
        Player player = players.get(turn % 2);
        player.getCards().remove(cardToReplace);
        updatePlayerCards(player, cardToReplace);
        tvReplace.setVisibility(View.GONE);
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }


    private boolean setNextCard(Player player, int x, int y) {
        Log.d(TAG, "setNextCard: " + gson.toJson(selectedCard));

        if (game[x][y].getCoin() != null && cardMapping.get(selectedCard.getNumber()).contains(-1)) {
//            ImageView ivCoin = ButterKnife.findById(view, R.id.ivCoin);
            updatePlayerCards(player, selectedCard);
//            ivCoin.setVisibility(View.GONE);
            game[x][y].setCoin(null);
            turn++;
            Log.d(TAG, "setNextCard: turn:- " + turn);
        } else if (board.assignCard(selectedCard, x, y, new Coin(player.getCoin().getColor()))) {
            //card_pos-1
//            ImageView ivCoin = ButterKnife.findById(view, R.id.ivCoin);
            Log.i(TAG, "onItemClick: selected card:- " + selectedCard.getNumber());
            if (selectedCard.getNumber() != 36 && selectedCard.getNumber() != 49) {
                updatePlayerCards(player, selectedCard);
                game[x][y].setCoin(new Coin(player.getCoin().getColor()));
                turn++;
                Log.d(TAG, "setNextCard: turn:- " + turn);
            } else {
                Utilities.instance.shortSnack(rlGameBoard, "Can't remove coin from empty position");
                return false;
            }
        }
        return true;

    }

    private void checkBorders(ArrayList<Integer> sequences) {
        if (sequences != null) {
            for (int i = 0; i < sequences.size(); i++) {
//                Log.d(TAG, "checkBorders:  sequence object:- " + gson.toJson(sequences));
                int x = sequences.get(i) / 10;
                int y = sequences.get(i) % 10;
                if (game[x][y].getCoin().getColor() == 2) {
                    game[x][y].setFilled(true);
                    game[x][y].setSelected(false);
//                    ivBorder.setBackgroundResource(R.drawable.border_filled_blue);
                } else if (game[x][y].getCoin().getColor() == 4) {
                    game[x][y].setFilled(true);
                    game[x][y].setSelected(false);
//                    ivBorder.setBackgroundResource(R.drawable.border_filled_green);
                } else {
                    game[x][y].setFilled(true);
                    game[x][y].setSelected(false);
//                    ivBorder.setBackgroundResource(R.drawable.border_filled_red);
                }
            }
            cardAdapter.updateData(game);
        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Player player = players.get(turn % NO_OF_PLAYERS);
        Log.d(TAG, "onItemClick: before:- " + gson.toJson(player));
        for (int i = 0; i < player.getCards().size(); i++) {
            player.getCards().get(i).setBordered(false);
        }
        Log.d(TAG, "onItemClick: intermediate:- " + gson.toJson(player));
        selectedCard = player.getCards().get(position);
        selectedCard.setBordered(true);
        Log.d(TAG, "onItemClick: after:- " + gson.toJson(player));
        ArrayList<Integer> positions = cardMapping.get(selectedCard.getNumber());
        if (availablePositions.size() != 0) {
            int i = 0;
            while (i < availablePositions.size()) {
                int pos = availablePositions.get(i);
                game[pos / 10][pos % 10].setSelected(false);
                i++;
            }
            availablePositions.clear();
        }
        int pos = positions.get(0);
        if (pos != -1 && pos != 100) {
            if (game[pos / 10][pos % 10].getCoin() == null) {
                game[pos / 10][pos % 10].setSelected(true);
                availablePositions.add(pos);
            }
            pos = positions.get(1);
            if (game[pos / 10][pos % 10].getCoin() == null) {
                game[pos / 10][pos % 10].setSelected(true);
                availablePositions.add(pos);
            }
            if (availablePositions.isEmpty()) {
                cardToReplace = selectedCard;
                replaceCardPos = position;
                tvReplace.setVisibility(View.VISIBLE);
            }
        } else if (pos == -1) {
            UpgradedAIPlayer aiPlayer = new UpgradedAIPlayer(board, 3, optionSize);
            aiPlayer.setPlayer(players.get(turn % NO_OF_PLAYERS));
            aiPlayer.setOpponent(players.get((turn + 1) % NO_OF_PLAYERS));
            ArrayList<Integer> def_pos = aiPlayer.findDefensivePositions(aiPlayer.convertBoardtoInt(game, turn % NO_OF_TEAMS, -1), turn % NO_OF_TEAMS, aiPlayer.createPositionValue());
            for (int p : def_pos) {
                if (game[p / 10][p % 10].getCoin() != null) {
                    game[p / 10][p % 10].setSelected(true);
                    availablePositions.add(p);
                }
            }
        } else if (pos == 100) {
            UpgradedAIPlayer aiPlayer = new UpgradedAIPlayer(board, 3, optionSize);
            aiPlayer.setPlayer(players.get(turn % NO_OF_PLAYERS));
            aiPlayer.setOpponent(players.get((turn + 1) % NO_OF_PLAYERS));
            ArrayList<Integer> def_pos = aiPlayer.findOffensivePositions(aiPlayer.convertBoardtoInt(game, turn % NO_OF_TEAMS, 1), turn % NO_OF_TEAMS, aiPlayer.createPositionValue());
            for (int p : def_pos) {
                if (game[p / 10][p % 10].getCoin() == null) {
                    game[p / 10][p % 10].setSelected(true);
                    availablePositions.add(p);
                }
            }
        }
        playerCardAdapter.updateData(player.getCards());
        cardAdapter.updateData(game);
//        lvPlayerCards.setAdapter(playerCardAdapter);
    }

    private void setupGame() {
        board = new Board();
        deck = new Deck(2);
        status = new Status();
        game = board.getGame();
        players.clear();
        turn = 0;
        computerPositions.clear();
        opponentPositions.clear();
        cardMapping = board.getCardMapping();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 9;


        ivRecentlyUsedCard.getLayoutParams().width = devicewidth;
        ivRecentlyUsedCard.getLayoutParams().height = (int) (devicewidth * 1.417);

        tvReplace.getLayoutParams().width = devicewidth;


//        Log.d(TAG, "onCreate: " + game.length * game[0].length);
//        Log.d(TAG, "onCreate: " + game[0][1].getCard().getType() + "\t" + game[0][1].getCard().getValue());

        cardAdapter = new CardAdapter(this, game);
        gvBoard.setAdapter(cardAdapter);

//        Log.d(TAG, "setupGame: deck size:- " + deck);
        distributeCards();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        lvPlayerCards.setLayoutManager(layoutManager);
//        playerCardListAdapter = new PlayerCardListAdapter(this, players.get(0).getCards());
//        lvPlayerCards.setAdapter(playerCardListAdapter);
        playerCardAdapter = new PlayerCardAdapter(this, this, players.get(0).getCards());
        lvPlayerCards.setAdapter(playerCardAdapter);
    }


    private void distributeCards() {
        for (int i = 0; i < NO_OF_PLAYERS; i++) {
            ArrayList<Card> cards = new ArrayList<>(6);
            for (int j = 0; j < 6; j++) {
                if (i == 0) {
                    Card card = deck.getShuffledCard();
//                    Card card = deck.getSequencedCard(counter1);
                    if (card != null) {
                        cards.add(card);
                    }
                } else {
                    Card card = deck.getShuffledCard();
//                    Card card = deck.getSequencedCard(counter2);
                    if (card != null) {
                        cards.add(card);
                    }
                }
            }
            Player player = new Player(cards, new Coin(((i % NO_OF_TEAMS) + 1) * 2));
            players.add(player);
        }
    }

    private void updatePlayerCards(Player player, Card selectedCard) {
        player.getCards().remove(selectedCard);
//                        player.getCards().add(deck.getShuffledCard());
//        if ((turn) % NO_OF_PLAYERS == 1) {
////            player.getCards().add(deck.getSequencedCard(counter2));
//            player.getCards().add(deck.getSequencedCard(counter2));
//        } else {
////            player.getCards().add(deck.getSequencedCard(counter2));
//            player.getCards().add(deck.getSequencedCard(counter1));
//        }
        Card card = deck.getShuffledCard();
        if (card != null) {
            player.getCards().add(card);
        }
//        phlayerCardListAdapter.updateData(players.get((turn + 1) % NO_OF_PLAYERS).getCards());
//        lvPlayerCards.setAdapter((playerCardListAdapter));
//        playerCardAdapter.updateData(players.get((turn + 1) % NO_OF_PLAYERS).getCards());
//        lvPlayerCards.setAdapter(playerCardAdapter);
    }

}