package com.incredible.sequence.Objects;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Board {

    private BoardCard[][] game;
    private ArrayList<Card> cards;
    String TAG = "Board";

    private BoardCard[][] createNewGame() {
        BoardCard[][] newBoard = new BoardCard[10][10];
        Deck deck = new Deck(1);
        cards = deck.getDeck();
        return newBoard;
    }

    public Board() {
        game = createNewGame();
        game = assignPositions(game);

        Log.d(TAG, "assignPositions: " + game[0][1].getCard().getType() + "\t" + game[0][1].getCard().getValue());

    }

    public Board(BoardCard[][] game) {
        this.game = game;
    }

    public HashMap<Integer, ArrayList<Integer>> getCardMapping(){
        HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
        // Clubs
        map.put(0, new ArrayList<>(Arrays.asList(80, 75)));
        map.put(1, new ArrayList<>(Arrays.asList(14, 36)));
        map.put(2, new ArrayList<>(Arrays.asList(13, 35)));
        map.put(3, new ArrayList<>(Arrays.asList(12, 34)));
        map.put(4, new ArrayList<>(Arrays.asList(11, 33)));
        map.put(5, new ArrayList<>(Arrays.asList(10, 32)));
        map.put(6, new ArrayList<>(Arrays.asList(20, 42)));
        map.put(7, new ArrayList<>(Arrays.asList(30, 52)));
        map.put(8, new ArrayList<>(Arrays.asList(40, 62)));
        map.put(9, new ArrayList<>(Arrays.asList(50, 72)));
        map.put(10, new ArrayList<>(Arrays.asList(100, 100)));
        map.put(11, new ArrayList<>(Arrays.asList(60, 73)));
        map.put(12, new ArrayList<>(Arrays.asList(70, 74)));
        // Diamonds
        map.put(13, new ArrayList<>(Arrays.asList(91, 76)));
        map.put(14, new ArrayList<>(Arrays.asList(59, 22)));
        map.put(15, new ArrayList<>(Arrays.asList(69, 23)));
        map.put(16, new ArrayList<>(Arrays.asList(79, 24)));
        map.put(17, new ArrayList<>(Arrays.asList(89, 25)));
        map.put(18, new ArrayList<>(Arrays.asList(98, 26)));
        map.put(19, new ArrayList<>(Arrays.asList(97, 27)));
        map.put(20, new ArrayList<>(Arrays.asList(96, 37)));
        map.put(21, new ArrayList<>(Arrays.asList(95, 47)));
        map.put(22, new ArrayList<>(Arrays.asList(94, 57)));
        map.put(23, new ArrayList<>(Arrays.asList(100, 100)));
        map.put(24, new ArrayList<>(Arrays.asList(93, 67)));
        map.put(25, new ArrayList<>(Arrays.asList(92, 77)));
        // Hearts
        map.put(26, new ArrayList<>(Arrays.asList(15, 46)));
        map.put(27, new ArrayList<>(Arrays.asList(87, 54)));
        map.put(28, new ArrayList<>(Arrays.asList(88, 55)));
        map.put(29, new ArrayList<>(Arrays.asList(78, 45)));
        map.put(30, new ArrayList<>(Arrays.asList(68, 44)));
        map.put(31, new ArrayList<>(Arrays.asList(58, 43)));
        map.put(32, new ArrayList<>(Arrays.asList(48, 53)));
        map.put(33, new ArrayList<>(Arrays.asList(38, 63)));
        map.put(34, new ArrayList<>(Arrays.asList(28, 64)));
        map.put(35, new ArrayList<>(Arrays.asList(18, 65)));
        map.put(36, new ArrayList<>(Arrays.asList(-1, -1)));
        map.put(37, new ArrayList<>(Arrays.asList(17, 66)));
        map.put(38, new ArrayList<>(Arrays.asList(16, 56)));
        // Spades
        map.put(39, new ArrayList<>(Arrays.asList(49, 21)));
        map.put(40, new ArrayList<>(Arrays.asList(1, 86)));
        map.put(41, new ArrayList<>(Arrays.asList(2, 85)));
        map.put(42, new ArrayList<>(Arrays.asList(3, 84)));
        map.put(43, new ArrayList<>(Arrays.asList(4, 83)));
        map.put(44, new ArrayList<>(Arrays.asList(5, 82)));
        map.put(45, new ArrayList<>(Arrays.asList(6, 81)));
        map.put(46, new ArrayList<>(Arrays.asList(7, 71)));
        map.put(47, new ArrayList<>(Arrays.asList(8, 61)));
        map.put(48, new ArrayList<>(Arrays.asList(19, 51)));
        map.put(49, new ArrayList<>(Arrays.asList(-1, -1)));
        map.put(50, new ArrayList<>(Arrays.asList(29, 41)));
        map.put(51, new ArrayList<>(Arrays.asList(39, 31)));

        return map;
    }


    private BoardCard[][] assignPositions(BoardCard[][] game) {
        // setting up all the jacks
        game[0][0] = new BoardCard(new Coin(-1), null, false, true);
        game[0][9] = new BoardCard(new Coin(-1), null, false, true);
        game[9][0] = new BoardCard(new Coin(-1), null, false, true);
        game[9][9] = new BoardCard(new Coin(-1), null, false, true);

//        Log.d(TAG, "assignPositions: assigning positions");

        // setting up all the spades
        game[0][1] = new BoardCard(null, cards.get(40), false, false);
        game[0][2] = new BoardCard(null, cards.get(41), false, false);
        game[0][3] = new BoardCard(null, cards.get(42), false, false);
        game[0][4] = new BoardCard(null, cards.get(43), false, false);
        game[0][5] = new BoardCard(null, cards.get(44), false, false);
        game[0][6] = new BoardCard(null, cards.get(45), false, false);
        game[0][7] = new BoardCard(null, cards.get(46), false, false);
        game[0][8] = new BoardCard(null, cards.get(47), false, false);
        game[1][9] = new BoardCard(null, cards.get(48), false, false);
        game[2][9] = new BoardCard(null, cards.get(50), false, false);
        game[3][9] = new BoardCard(null, cards.get(51), false, false);
        game[4][9] = new BoardCard(null, cards.get(39), false, false);

// setting up all the diamonds
        game[5][9] = new BoardCard(null, cards.get(14), false, false);
        game[6][9] = new BoardCard(null, cards.get(15), false, false);
        game[7][9] = new BoardCard(null, cards.get(16), false, false);
        game[8][9] = new BoardCard(null, cards.get(17), false, false);
        game[9][8] = new BoardCard(null, cards.get(18), false, false);
        game[9][7] = new BoardCard(null, cards.get(19), false, false);
        game[9][6] = new BoardCard(null, cards.get(20), false, false);
        game[9][5] = new BoardCard(null, cards.get(21), false, false);
        game[9][4] = new BoardCard(null, cards.get(22), false, false);
        game[9][3] = new BoardCard(null, cards.get(24), false, false);
        game[9][2] = new BoardCard(null, cards.get(25), false, false);
        game[9][1] = new BoardCard(null, cards.get(13), false, false);

// setting up all the clubs
        game[1][4] = new BoardCard(null, cards.get(1), false, false);
        game[1][3] = new BoardCard(null, cards.get(2), false, false);
        game[1][2] = new BoardCard(null, cards.get(3), false, false);
        game[1][1] = new BoardCard(null, cards.get(4), false, false);
        game[1][0] = new BoardCard(null, cards.get(5), false, false);
        game[2][0] = new BoardCard(null, cards.get(6), false, false);
        game[3][0] = new BoardCard(null, cards.get(7), false, false);
        game[4][0] = new BoardCard(null, cards.get(8), false, false);
        game[5][0] = new BoardCard(null, cards.get(9), false, false);
        game[6][0] = new BoardCard(null, cards.get(11), false, false);
        game[7][0] = new BoardCard(null, cards.get(12), false, false);
        game[8][0] = new BoardCard(null, cards.get(0), false, false);

// setting up all hearts
        game[1][5] = new BoardCard(null, cards.get(26), false, false);
        game[1][6] = new BoardCard(null, cards.get(38), false, false);
        game[1][7] = new BoardCard(null, cards.get(37), false, false);
        game[1][8] = new BoardCard(null, cards.get(35), false, false);
        game[2][8] = new BoardCard(null, cards.get(34), false, false);
        game[3][8] = new BoardCard(null, cards.get(33), false, false);
        game[4][8] = new BoardCard(null, cards.get(32), false, false);
        game[5][8] = new BoardCard(null, cards.get(31), false, false);
        game[6][8] = new BoardCard(null, cards.get(30), false, false);
        game[7][8] = new BoardCard(null, cards.get(29), false, false);
        game[8][8] = new BoardCard(null, cards.get(28), false, false);
        game[8][7] = new BoardCard(null, cards.get(27), false, false);

// setting up all the spades
        game[8][6] = new BoardCard(null, cards.get(40), false, false);
        game[8][5] = new BoardCard(null, cards.get(41), false, false);
        game[8][4] = new BoardCard(null, cards.get(42), false, false);
        game[8][3] = new BoardCard(null, cards.get(43), false, false);
        game[8][2] = new BoardCard(null, cards.get(44), false, false);
        game[8][1] = new BoardCard(null, cards.get(45), false, false);
        game[7][1] = new BoardCard(null, cards.get(46), false, false);
        game[6][1] = new BoardCard(null, cards.get(47), false, false);
        game[5][1] = new BoardCard(null, cards.get(48), false, false);
        game[4][1] = new BoardCard(null, cards.get(50), false, false);
        game[3][1] = new BoardCard(null, cards.get(51), false, false);
        game[2][1] = new BoardCard(null, cards.get(39), false, false);

// setting up all the diamonds
        game[2][2] = new BoardCard(null, cards.get(14), false, false);
        game[2][3] = new BoardCard(null, cards.get(15), false, false);
        game[2][4] = new BoardCard(null, cards.get(16), false, false);
        game[2][5] = new BoardCard(null, cards.get(17), false, false);
        game[2][6] = new BoardCard(null, cards.get(18), false, false);
        game[2][7] = new BoardCard(null, cards.get(19), false, false);
        game[3][7] = new BoardCard(null, cards.get(20), false, false);
        game[4][7] = new BoardCard(null, cards.get(21), false, false);
        game[5][7] = new BoardCard(null, cards.get(22), false, false);
        game[6][7] = new BoardCard(null, cards.get(24), false, false);
        game[7][7] = new BoardCard(null, cards.get(25), false, false);
        game[7][6] = new BoardCard(null, cards.get(13), false, false);

// setting up all the clubs
        game[7][5] = new BoardCard(null, cards.get(0), false, false);
        game[7][4] = new BoardCard(null, cards.get(12), false, false);
        game[7][3] = new BoardCard(null, cards.get(11), false, false);
        game[7][2] = new BoardCard(null, cards.get(9), false, false);
        game[6][2] = new BoardCard(null, cards.get(8), false, false);
        game[5][2] = new BoardCard(null, cards.get(7), false, false);
        game[4][2] = new BoardCard(null, cards.get(6), false, false);
        game[3][2] = new BoardCard(null, cards.get(5), false, false);
        game[3][3] = new BoardCard(null, cards.get(4), false, false);
        game[3][4] = new BoardCard(null, cards.get(3), false, false);
        game[3][5] = new BoardCard(null, cards.get(2), false, false);
        game[3][6] = new BoardCard(null, cards.get(1), false, false);

// setting up all hearts
        game[4][6] = new BoardCard(null, cards.get(26), false, false);
        game[5][6] = new BoardCard(null, cards.get(38), false, false);
        game[6][6] = new BoardCard(null, cards.get(37), false, false);
        game[6][5] = new BoardCard(null, cards.get(35), false, false);
        game[6][4] = new BoardCard(null, cards.get(34), false, false);
        game[6][3] = new BoardCard(null, cards.get(33), false, false);
        game[5][3] = new BoardCard(null, cards.get(32), false, false);
        game[4][3] = new BoardCard(null, cards.get(31), false, false);
        game[4][4] = new BoardCard(null, cards.get(30), false, false);
        game[4][5] = new BoardCard(null, cards.get(29), false, false);
        game[5][5] = new BoardCard(null, cards.get(28), false, false);
        game[5][4] = new BoardCard(null, cards.get(27), false, false);

        return game;
    }

    public BoardCard[][] getGame() {
        return game;
    }

    public boolean assignCard(Card card, int x, int y, Coin coin) {
        if ((game[x][y].getCoin() == null && !game[x][y].isJack()) || (game[x][y].getCoin() != null && !game[x][y].isJack() && (card.getNumber() == 36 || card.getNumber() == 49 ) && !game[x][y].isFilled())) {
            Log.d(TAG, "assignCard: true");
            return true;
        } else {
            return false;
        }
    }

}
