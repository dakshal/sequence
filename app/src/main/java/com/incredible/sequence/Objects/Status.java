package com.incredible.sequence.Objects;

import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class Status {

    public Player isGameOver(BoardCard[][] game, Player player) {
        int count = player.getNoOfSequences();

        ArrayList<Integer> sequences = player.getSequences();

        int color = player.getCoin().getColor();
        Log.d(TAG, "isGameOver: count = " + count + "\tplayercolor = " + color);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j < 6) {   // horizontal
                    if (game[i][j].getCoin() != null && game[i][j + 1].getCoin() != null && game[i][j + 2].getCoin() != null && game[i][j + 3].getCoin() != null && game[i][j + 4].getCoin() != null) {
                        ArrayList<Integer> coinColor = new ArrayList<>();
                        for (int k = 0; k < 5; k++) {
                            if (!coinColor.contains(game[i][j + k].getCoin().getColor()))
                                coinColor.add(game[i][j + k].getCoin().getColor());
                        }
                        if ((coinColor.size() == 1 || (coinColor.size() == 2 && coinColor.contains(-1))) && coinColor.contains(color)) {
                            int score = 0;
                            for (int k = 0; k < 5; k++) {
                                if (sequences.contains((i) * 10 + (j + k))) {
                                    score += color / 2;
                                } else {
                                    score += color;
                                }
                            }
                            Log.d(TAG, "isGameOver: horizontal\t" + color + "\ti=" + i + "\tj=" + j + "\tscore=" + score);
                            if (score >= ((color * 4.5))) {
                                for (int k = 0; k < 5; k++) {
                                    sequences.add(i * 10 + (j + k));
                                }
                                count += 1;
                            }
                        }
                    }
                }
                if (i < 6) {   // vertical
                    if (game[i][j].getCoin() != null && game[i + 1][j].getCoin() != null && game[i + 2][j].getCoin() != null && game[i + 3][j].getCoin() != null && game[i + 4][j].getCoin() != null) {
                        ArrayList<Integer> coinColor = new ArrayList<>();
                        for (int k = 0; k < 5; k++) {
                            if (!coinColor.contains(game[i + k][j].getCoin().getColor()))
                                coinColor.add(game[i + k][j].getCoin().getColor());
                        }
                        if ((coinColor.size() == 1 || (coinColor.size() == 2 && coinColor.contains(-1))) && coinColor.contains(color)) {
                            int score = 0;
                            for (int k = 0; k < 5; k++) {
                                if (sequences.contains((i + k) * 10 + j)) {
                                    score += color / 2;
                                } else {
                                    score += color;
                                }
                            }
                            Log.d(TAG, "isGameOver: vertical\t" + color + "\ti=" + i + "\tj=" + j + "\tscore=" + score);
                            if (score >= ((color * 4.5))) {
                                for (int k = 0; k < 5; k++) {
                                    sequences.add((i + k) * 10 + j);
                                }
                                count += 1;
                            }
                        }
                    }
                }
                if (i < 6 && j < 6) { //diagonals
                    if (game[i][j].getCoin() != null && game[i + 1][j + 1].getCoin() != null && game[i + 2][j + 2].getCoin() != null && game[i + 3][j + 3].getCoin() != null && game[i + 4][j + 4].getCoin() != null) {
                        ArrayList<Integer> coinColor = new ArrayList<>();
                        for (int k = 0; k < 5; k++) {
                            if (!coinColor.contains(game[i + k][j + k].getCoin().getColor()))
                                coinColor.add(game[i + k][j + k].getCoin().getColor());
                        }
                        if ((coinColor.size() == 1 || (coinColor.size() == 2 && coinColor.contains(-1))) && coinColor.contains(color)) {
                            int score = 0;
                            for (int k = 0; k < 5; k++) {
                                if (sequences.contains((i + k) * 10 + (j + k))) {
                                    score += color / 2;
                                } else {
                                    score += color;
                                }
                            }
                            Log.d(TAG, "isGameOver: diagonal\t" + color + "\ti=" + i + "\tj=" + j + "\tscore=" + score);
                            if (score >= ((color * 4.5f))) {
                                for (int k = 0; k < 5; k++) {
                                    sequences.add((i + k) * 10 + (j + k));
                                }
                                count += 1;
                            }
                        }
                    }

                    if (game[i + 4][j].getCoin() != null && game[i + 3][j + 1].getCoin() != null && game[i + 2][j + 2].getCoin() != null && game[i + 1][j + 3].getCoin() != null && game[i][j + 4].getCoin() != null) {
                        ArrayList<Integer> coinColor = new ArrayList<>();
                        for (int k = 0; k < 5; k++) {
                            if (!coinColor.contains(game[i + (4 - k)][j + k].getCoin().getColor()))
                                coinColor.add(game[i + (4 - k)][j + k].getCoin().getColor());
                        }
                        if ((coinColor.size() == 1 || (coinColor.size() == 2 && coinColor.contains(-1))) && coinColor.contains(color)) {
                            int score = 0;
                            for (int k = 0; k < 5; k++) {
                                if (sequences.contains((i + (4 - k)) * 10 + (j + k))) {
                                    score += color / 2;
                                } else {
                                    score += color;
                                }
                            }
                            Log.d(TAG, "isGameOver: diagonal\t" + color + "\ti=" + i + "\tj=" + j + "\tscore=" + score);
                            if (score >= ((color * 4.5f))) {
                                for (int k = 0; k < 5; k++) {
                                    sequences.add((i + (4 - k)) * 10 + (j + k));
                                }
                                count += 1;
                            }
                        }
                    }
                }
            }
        }

        Log.d(TAG, "isGameOver: score>2 " + count);

        player.setNoOfSequences(count);
        player.setSequences(sequences);
        return player;
    }

    public int isGameOver(int[][] game, Player player, Player opponent, ArrayList<ArrayList<Integer>> sequences) {
        int pl = 0;
        int cl = 0;
        for (ArrayList<Integer> seq : sequences) {
            int playerCount = 0;
            int computerCount = 0;
            for (int i : seq) {
                if (i == 0 || i == 9 || i == 90 || i == 99) {
                    computerCount++;
                    playerCount++;
                    continue;
                }
                if (game[i / 10][i % 10] == 1) {
                    playerCount++;
                } else if (game[i / 10][i % 10] == -1) {
                    computerCount++;
                }
            }

            if (computerCount == 5) cl++;
            else if (playerCount == 5) pl++;
        }
        return Integer.parseInt(cl + "" + pl);
    }


    public boolean isSequence(Board board, BoardCard[] cards) {

        BoardCard[][] game = board.getGame();
        return false;
    }

}
