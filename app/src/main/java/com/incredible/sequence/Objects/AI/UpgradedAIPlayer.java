package com.incredible.sequence.Objects.AI;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.incredible.sequence.Objects.Board;
import com.incredible.sequence.Objects.BoardCard;
import com.incredible.sequence.Objects.Card;
import com.incredible.sequence.Objects.Player;
import com.incredible.sequence.Objects.Status;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by dakshal on 12/21/2017.
 */

public class UpgradedAIPlayer {

    private ArrayList<ArrayList<Integer>> totalSequences = new ArrayList<>();
    //    private HashMap<Integer, ArrayList<Integer>> positionSequences = new HashMap<>();
    public static final String TAG = "UpgradedAIPlayer";
    private Board board;
    private HashMap<Integer, ArrayList<Integer>> cardMapping;
    private Status status;
    private Player player;
    private Player opponent;
    private int uptoDepth = 10;
    private BoardCard[][] game;
    ArrayList<CardAndScores> rootChildScores = new ArrayList<>();
    ArrayList<Integer> playerPositions = new ArrayList<>();
    ArrayList<Integer> opponentPositions = new ArrayList<>();
    BoardCard[][] newGame;
    Gson gson = new Gson();
    private int optionSize = 10;

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

    public void createSequences() {
        totalSequences.clear();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j < 6) {
                    ArrayList<Integer> positions = new ArrayList<>(5);
                    for (int k = 0; k < 5; k++) {
                        positions.add((i * 10) + (j + k));
                    }
                    totalSequences.add(positions);
                }
                if (i < 6) {
                    ArrayList<Integer> positions = new ArrayList<>(5);
                    for (int k = 0; k < 5; k++) {
                        positions.add(((i + k) * 10) + j);
                    }
                    totalSequences.add(positions);
                }
                if (i < 6 && j < 6) {
                    ArrayList<Integer> positions = new ArrayList<>(5);
                    for (int k = 0; k < 5; k++) {
                        positions.add(((i + k) * 10) + (j + k));
                    }
                    totalSequences.add(positions);
                }
                if ((i + j) >= 4 && (i + j) <= 14 && ((i + 4) < 10) && ((j + 4) < 10)) {
                    ArrayList<Integer> positions = new ArrayList<>(5);
                    for (int k = 0; k < 5; k++) {
                        positions.add(((i + (4 - k)) * 10) + (j + k));
                    }
                    totalSequences.add(positions);
                }
            }
        }
    }

    public HashMap<Integer, ArrayList<Integer>> createPositionValue() {
        HashMap<Integer, ArrayList<Integer>> positionSequences = new HashMap<>();
        if (totalSequences.size() == 0 || totalSequences == null || totalSequences.isEmpty())
            createSequences();

        for (int i = 0; i < 100; i++) {
            ArrayList<Integer> sequences = new ArrayList<>();
            for (int j = 0; j < totalSequences.size(); j++) {
                if (totalSequences.get(j).contains(i)) {
                    sequences.add(j);
                }
            }
            positionSequences.put(i, sequences);
        }
        return positionSequences;
    }

    public UpgradedAIPlayer(Board board, int uptoDepth, int optionSize) {
        this.board = board;
        this.uptoDepth = uptoDepth;
        this.optionSize = optionSize;
        game = board.getGame();
        cardMapping = board.getCardMapping();
        status = new Status();
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public AICard nextMove(Player player, Player opponent, ArrayList<Integer> playerPositions, ArrayList<Integer> opponentPositions, Context context) {
        this.player = player;
        this.opponent = opponent;
        this.playerPositions.clear();
        this.opponentPositions.clear();
        this.playerPositions.addAll(playerPositions);
        this.opponentPositions.addAll(opponentPositions);
        int[][] convertedGame = convertBoardtoInt(board.getGame(), 1, -1);
        alphaBetaMinimax(Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 1, player.getCards(), new ArrayList<Card>(), convertedGame);

        int pos = getBestMove();

        Log.d(TAG, "nextStep: " + pos);
        Status status = new Status();
        Player playerUpdated = status.isGameOver(game, player);
        Log.d(TAG, "nextStep: player:- " + gson.toJson(playerUpdated));
        Log.d(TAG, "nextStep: game:- " + gson.toJson(game));
        Log.d(TAG, "nextStep: newGame:- " + gson.toJson(newGame));
        Log.d(TAG, "nextStep: rootChildScores:- " + gson.toJson(rootChildScores));
        Card playerCard = null;

        for (Card card : player.getCards()) {
            if (cardMapping.get(card.getNumber()).contains(pos)) {
                playerCard = card;
            }
            if (game[pos / 10][pos % 10].getCoin() != null && cardMapping.get(card.getNumber()).contains(-1)) {
                playerCard = card;
            }
        }
        if (playerCard == null) {
            for (Card card : player.getCards()) {
                if (cardMapping.get(card.getNumber()).contains(100)) {
                    playerCard = card;
                }
            }
        }

        if (playerCard == null) {
            Toast.makeText(context, "Selecting Random Card", Toast.LENGTH_SHORT).show();
            AICard aiCard = randomCardSelector(player);
            return aiCard;
        }

        AICard aiCard = new AICard(pos / 10, pos % 10, pos, playerCard);
        return aiCard;
    }

    public AICard randomCardSelector(Player player) {
        Random random = new Random();
        HashMap<Integer, ArrayList<Integer>> positionSequences = createPositionValue();

        boolean selected = false;
        while (!selected) {
            int cardNumber = random.nextInt(player.getCards().size());
            Card playerCard = player.getCards().get(cardNumber);
            ArrayList<Integer> indexes = cardMapping.get(playerCard.getNumber());
            int select = indexes.get(0);
            if (select == -1) {
                ArrayList<Integer> def_pos = findDefensivePositions(convertBoardtoInt(game, 1, -1), 1, positionSequences);
                selected = true;
                select = def_pos.get(0);
                AICard aiCard = new AICard(select / 10, select % 10, def_pos.get(0), playerCard);
                return aiCard;
            } else if (select == 100) {
                ArrayList<Integer> off_pos = findOffensivePositions(convertBoardtoInt(game, 1, 1), 1, positionSequences);
                selected = true;
                select = off_pos.get(0);
                AICard aiCard = new AICard(select / 10, select % 10, off_pos.get(0), playerCard);
                return aiCard;
            }
            if (game[select / 10][select % 10].getCoin() == null) {
                selected = true;
                AICard aiCard = new AICard(select / 10, select % 10, select, playerCard);
                return aiCard;
            }
            select = indexes.get(1);
            if (game[select / 10][select % 10].getCoin() == null) {
                selected = true;
                AICard aiCard = new AICard(select / 10, select % 10, select, playerCard);
                return aiCard;
            }
        }
        return null;
    }


    private int getBestMove() {
        int max = Integer.MIN_VALUE;
        int best = 0;
        for (int i = 0; i < rootChildScores.size(); i++) {
            if (max < rootChildScores.get(i).getScore()) {
                Log.d(TAG, "getBestMove: position:- " + rootChildScores.get(i).getPoint() + "\t\tscore:- " + rootChildScores.get(i).getScore());
                max = rootChildScores.get(i).getScore();
                best = rootChildScores.get(i).getPoint();
            }
        }
        Log.d(TAG, "getBestMove: ");
        return best;
    }


    public int alphaBetaMinimax(int alpha, int beta, int depth, int turn, ArrayList<Card> playerCards, ArrayList<Card> usedCards, int[][] convertedGame) {

        if (beta <= alpha) {
            if (turn == 1) {
                return Integer.MAX_VALUE;
            } else {
                return Integer.MIN_VALUE;
            }
        }

        int gameStatus = status.isGameOver(convertedGame, player, opponent, totalSequences);
        HashMap<Integer, ArrayList<Integer>> positionSequences = createPositionValue();

        if (depth == uptoDepth || gameStatus >= 20 || gameStatus == 2 || gameStatus == 12) {
            printTable(convertedGame, "testing convertedGame\t");
            return evaluateBoard(convertedGame, turn, positionSequences);
        }

        ArrayList<Integer> pointsAvailable = getAvailableStates(convertedGame, playerCards, usedCards);

        if (pointsAvailable.isEmpty()) {
            return 0;
        }

        if (depth == 0) {
            rootChildScores.clear();
        }

        int maxValue = Integer.MIN_VALUE, minValue = Integer.MAX_VALUE;

        int currentScore = 0;

        if (turn == 1) {

            for (int i = 0; i < pointsAvailable.size(); i++) {
//                new Thread(new R)
                int point = pointsAvailable.get(i);
                Card playerCard = null;
                for (int j = 0; j < playerCards.size(); j++) {
//                    Log.d(TAG, "alphaBetaMinimax: " + j);
                    if (cardMapping.get(playerCards.get(j).getNumber()).contains(point)) {
                        playerCard = playerCards.get(j);
                        break;
                    }
                }
//                Log.e(TAG, "alphaBetaMinimax: depth:- " + depth);
                point = placeAMove(point, convertedGame, 1, positionSequences);
                if (point < 0)
                    continue;
                positionSequences = updatePossibleSequences(convertedGame, positionSequences);
                usedCards.add(playerCard);
                currentScore = alphaBetaMinimax(alpha, beta, depth + 1, 2, playerCards, usedCards, convertedGame);
                maxValue = Math.max(maxValue, currentScore);

                //Set alpha
                alpha = Math.max(currentScore, alpha);

                if (depth == 0)
                    rootChildScores.add(new CardAndScores(currentScore, point));

                if (cardMapping.get(playerCard.getNumber()).contains(-1)) {
                    convertedGame[point / 10][point % 10] = -1;
                } else {
                    convertedGame[point / 10][point % 10] = 0;
                }
                usedCards.remove(playerCard);

                //If a pruning has been done, don't evaluate the rest of the sibling states
                if (currentScore == Integer.MAX_VALUE || currentScore == Integer.MIN_VALUE) break;

            }
        } else if (turn == 2) {
            int point = 0;
            point = placeAMove(point, convertedGame, 2, positionSequences);
            positionSequences = updatePossibleSequences(convertedGame, positionSequences);
            currentScore = alphaBetaMinimax(alpha, beta, depth + 1, 1, playerCards, usedCards, convertedGame);
            minValue = Math.min(minValue, currentScore);

            //Set beta
            beta = Math.min(currentScore, beta);
            convertedGame[point / 10][point % 10] = 0;
//            usedCards.remove(playerCard);
        }
//            //reset board

//                Log.e(TAG, "alphaBetaMinimax: removing at point:-- " + point + "\tturn:- " + turn);

        return turn == 1 ? maxValue : minValue;
    }

    public int placeAMove(int point, int[][] state, int turn, HashMap<Integer, ArrayList<Integer>> positionSequences) {
        if (turn == 1) {     // Computer turn
            if (point == -1) {
                ArrayList<Integer> points = findDefensivePositions(state, turn, positionSequences);
                if (points.size() > 0 || !points.isEmpty()) {
                    point = points.get(0);
                    if (state[point / 10][point % 10] != 0) {
                        state[point / 10][point % 10] = 0;
                    }
                } else {
                    return -10;
                }
            } else if (point == 100) {
                point = findOffensivePositions(state, turn, positionSequences).get(0);
                state[point / 10][point % 10] = 1;
            } else {
                state[point / 10][point % 10] = 1;
            }

        } else if (turn == 2) {       // Player turn
            point = findOffensivePositions(state, turn, positionSequences).get(0);
            state[point / 10][point % 10] = -1;
        }
        printTable(state, "placeAMove testing\t");
        Log.e(TAG, "placeAMove: placing at point:-- " + point + "\tturn+" + turn);
        return point;
    }

    public HashMap<Integer, ArrayList<Integer>> updatePossibleSequences(int[][] state, HashMap<Integer, ArrayList<Integer>> positionSequences) {
        // TODO implement functionality to remove affecting sequences by the creation of one sequence

        ArrayList<Integer> createdPos = new ArrayList<>();
        ArrayList<Integer> createdSeqs = new ArrayList<>();

        for (int i = 0; i < totalSequences.size(); i++) {
            int count = 0;
            ArrayList<Integer> seq = totalSequences.get(i);
//            Log.d(TAG, "updatePossibleSequences: " + gson.toJson(seq));
            for (int j = 0; j < seq.size(); j++) {

                if (state[seq.get(j) / 10][seq.get(j) % 10] == 1) {
                    count++;
                } else if (state[seq.get(j) / 10][seq.get(j) % 10] == -1) {
                    count--;
                }
            }
            if (count == 5 || count == -5) {
                createdPos.addAll(seq);
                createdSeqs.add(i);
            } else if ((count == 4 || count == -4) && (seq.contains(0) || seq.contains(9) || seq.contains(90) || seq.contains(99))) {
                createdPos.addAll(seq);
                createdSeqs.add(i);
            }
        }

        ArrayList<ArrayList<Integer>> updatedSeqs = new ArrayList<>();

        for (int i = 0; i < totalSequences.size(); i++) {
            int count = 0;
            ArrayList<Integer> seq = totalSequences.get(i);
            for (int j = 0; j < seq.size(); j++) {
                if (createdPos.contains(seq.get(j))) {
                    count++;
                }
            }
            if (count < 2) {
                updatedSeqs.add(seq);
            }
        }

        positionSequences.clear();

        for (int i = 0; i < 100; i++) {
            ArrayList<Integer> sequences = new ArrayList<>();
            for (int j = 0; j < updatedSeqs.size(); j++) {
                if (updatedSeqs.get(j).contains(i)) {
                    sequences.add(j);
                }
            }
            positionSequences.put(i, sequences);
        }
        return positionSequences;
    }

    public ArrayList<Integer> findOffensivePositions(int[][] positionValue, int turn, HashMap<Integer, ArrayList<Integer>> positionSequences) {

        int[][] threats = new int[10][10];

        for (int i = 0; i < 100; i++) {
            if (i == 0 || i == 9 || i == 90 || i == 99) continue;
            if (positionValue[i / 10][i % 10] != 0) continue;

            ArrayList<Integer> posSeqs = positionSequences.get(i);

            for (int j = 0; j < posSeqs.size(); j++) {
                ArrayList<Integer> sequence = totalSequences.get(posSeqs.get(j));
                int co = 0, pl = 0;
                for (int k = 0; k < sequence.size(); k++) {
                    int p = sequence.get(k);
                    if (p == 0 || p == 9 || p == 90 || p == 99) {
                        co++;
                        pl++;
                        continue;
                    }
                    if (positionValue[p / 10][p % 10] == 1) {
                        co++;
                    } else if (positionValue[p / 10][p % 10] == -1) {
                        pl++;
                    }
                }
                if (sequence.contains(0) || sequence.contains(9) || sequence.contains(90) || sequence.contains(99)) {
                    if (co > pl) {
                        pl--;
                    } else if (pl > co) {
                        co--;
                    } else {
                        co--;
                        pl--;
                    }
                }
                if (turn % 2 == 0) {
                    if (co - pl < 0) threats[i / 10][i % 10] += Math.pow(3, Math.abs(co - pl));
                    else if (pl - co < 0) threats[i / 10][i % 10] -= Math.pow(3, Math.abs(co - pl));
                } else {
                    if (co - pl > 0) threats[i / 10][i % 10] += Math.pow(3, Math.abs(co - pl));
                    else if (pl - co > 0) threats[i / 10][i % 10] -= Math.pow(3, Math.abs(co - pl));
                }
            }
        }

        printTable(positionValue, "offensive threats positionTable: \t\t");

        Log.d(TAG, "threats: \t\t----------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            String msg = "";
            for (int j = 0; j < 10; j++) {
                msg += "\t" + threats[i][j];
            }
            Log.d(TAG, "threats: \t\t" + msg);
        }
        Log.d(TAG, "threats: \t\t----------------------------------------------------");

        ArrayList<Integer> positions = new ArrayList<>();

        for (int k = 0; k < optionSize; k++) {
            int position = -1;
            float value = -1000;
            for (int i = 0; i < 100; i++) {
                if (threats[i / 10][i % 10] > value && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == 0) {
                    value = threats[i / 10][i % 10];
                    position = i;
                }
            }
            if (positions.contains(position)) {
                continue;
            }
            if (position == -1) {
                break;
            }
            positions.add(position);
            threats[position / 10][position % 10] = Integer.MIN_VALUE;
        }

//        Log.d(TAG, "findOffensivePositions: " + position + "\tval:- " + value);

        return positions;
    }

    public ArrayList<Integer> findDefensivePositions(int[][] positionValue, int turn, HashMap<Integer, ArrayList<Integer>> positionSequences) {
        int[][] threats = new int[10][10];
        ArrayList<Integer> allSeq = new ArrayList<>();
        ArrayList<Integer> allpos = new ArrayList<>();

        allSeq.addAll(player.getSequences());
        allSeq.addAll(opponent.getSequences());

        for (int i : allSeq) {
            allpos.addAll(totalSequences.get(i));
        }

        for (int i = 0; i < 100; i++) {
            if (i == 0 || i == 9 || i == 90 || i == 99) continue;
            if (positionValue[i / 10][i % 10] == 0) continue;

            if (allpos.contains(i)) continue;

            ArrayList<Integer> posSeq = positionSequences.get(i);

            for (int j = 0; j < posSeq.size(); j++) {
                ArrayList<Integer> sequence = totalSequences.get(posSeq.get(j));
                int co = 0, pl = 0;
                for (int k = 0; k < sequence.size(); k++) {
                    int p = sequence.get(k);
                    if (p == 0 || p == 9 || p == 90 || p == 99) {
                        co++;
                        pl++;
                        continue;
                    }
                    if (positionValue[p / 10][p % 10] == 1) {
                        co++;
                    } else if (positionValue[p / 10][p % 10] == -1) {
                        pl++;
                    }
                }
                if (sequence.contains(0) || sequence.contains(9) || sequence.contains(90) || sequence.contains(99)) {
                    if (co > pl) {
                        pl--;
                    } else if (pl > co) {
                        co--;
                    } else {
                        co--;
                        pl--;
                    }
                }
                if (turn % 2 == 1) {
                    if (co - pl < 0) threats[i / 10][i % 10] += Math.pow(3, Math.abs(co - pl));
                    else if (pl - co < 0) threats[i / 10][i % 10] -= Math.pow(3, Math.abs(co - pl));
                } else {
                    if (co - pl > 0) threats[i / 10][i % 10] += Math.pow(3, Math.abs(co - pl));
                    else if (pl - co > 0) threats[i / 10][i % 10] -= Math.pow(3, Math.abs(co - pl));
                }
            }
        }

        printTable(positionValue, "defensive threats positionTable: \t\t");

        Log.d(TAG, "defensive threats: \t\t----------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            String msg = "";
            for (int j = 0; j < 10; j++) {
                msg += "\t" + threats[i][j];
            }
            Log.d(TAG, "defensive threats: \t\t" + msg);
        }
        Log.d(TAG, "defensive threats: \t\t----------------------------------------------------");

        ArrayList<Integer> positions150 = new ArrayList<>();
        ArrayList<Integer> positions100 = new ArrayList<>();
        ArrayList<Integer> positions50 = new ArrayList<>();
        ArrayList<Integer> positions0 = new ArrayList<>();

//        for (int k = 0; k < optionSize; k++) {
//            int position = -1;
//            float value = -10000;

        for (int i = 0; i < 100; i++) {
            if (threats[i / 10][i % 10] > 150 && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == -1) {
                positions150.add(i);
            }
            if (threats[i / 10][i % 10] > 100 && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == -1) {
                positions100.add(i);
            }
            if (threats[i / 10][i % 10] > 50 && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == -1) {
                positions50.add(i);
            }
            if (threats[i / 10][i % 10] >= 0 && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == -1) {
                positions0.add(i);
            }
//            }
//            if (positions.contains(position)) {
//                continue;
//            }
//            if (position == -1) {
//                break;
//            }
//            threats[position / 10][position % 10] = Integer.MIN_VALUE;
        }

//        Log.d(TAG, "findOffensivePositions: " + position + "\tval:- " + value);

//        if (positions150.size() > 0) {
            return positions150;
//        } else if (positions100.size() > 0) {
//            return positions100;
//        } else if (positions50.size() > 0) {
//            return positions50;
//        } else {
//            return positions0;
//        }

    }

    void printTable(int[][] positionValue, String tag) {
        Log.d(TAG, tag + ": \t\t----------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            Log.d(TAG, tag + ": \t\t" + gson.toJson(positionValue[i]));
        }
        Log.d(TAG, tag + ": \t\t----------------------------------------------------");
    }

    public int[][] convertBoardtoInt(BoardCard[][] state, int turn, int offense) {
        int positionValue[][] = new int[10][10];

        int playerCoin = player.getCoin().getColor();
        int opponentCoin = opponent.getCoin().getColor();

        if (turn != 1) {
            playerCoin += opponentCoin;
            opponentCoin = playerCoin - opponentCoin;
            playerCoin = playerCoin - opponentCoin;
        }
        int x, y;
        if (offense == 1) {
            for (int i = 0; i < player.getCards().size(); i++) {
                ArrayList<Integer> positions = board.getCardMapping().get(player.getCards().get(i).getNumber());
                if (positions.contains(-1) || positions.contains(100)) {
                    continue;
                }
                for (int pos : positions) {
                    positionValue[pos / 10][pos % 10] = 1;
                }
            }
        }

        for (int i = 0; i < 100; i++) {
            x = i / 10;
            y = i % 10;
            if (state[x][y].getCoin() != null) {
                if (state[x][y].getCoin().getColor() == playerCoin) {
                    positionValue[x][y] = 1;
                } else if (state[x][y].getCoin().getColor() == opponentCoin) {
                    positionValue[x][y] = -1;
                } else {
                    positionValue[x][y] = offense;
                }
            }
        }

        return positionValue;
    }

    private ArrayList<Integer> getAvailableStates(int[][] game, ArrayList<Card> playerCards, ArrayList<Card> usedCards) {
        ArrayList<Integer> positions = new ArrayList<>();

        for (Card playerCard : playerCards) {
            ArrayList<Integer> cardPositions = cardMapping.get(playerCard.getNumber());
            if (!cardPositions.contains(-1) && !cardPositions.contains(100) && !usedCards.contains(playerCard)) {
                for (int position : cardPositions) {
                    if (game[position / 10][position % 10] == 0 && !positions.contains(position)) {
                        positions.add(position);
                    }
                }
            } else if (cardPositions.contains(-1) || cardPositions.contains(100)) {
                positions.add(cardPositions.get(0));
            }
        }

        return positions;
    }

    private int evaluateBoard(int[][] positionValue, int turn, HashMap<Integer, ArrayList<Integer>> positionSequences) {
        int boardScore = 0;

        for (int i = 0; i < totalSequences.size(); i++) {
            if (player.getSequences().contains(i) || opponent.getSequences().contains(i))
                continue;

            // TODO implment functionality to skipp weightage of already made sequences
            ArrayList<Integer> sequence = totalSequences.get(i);
            int co = 0, pl = 0;
            for (int k = 0; k < sequence.size(); k++) {
                int p = sequence.get(k);
                if (p == 0 || p == 9 || p == 90 || p == 99) {
                    co++;
                    pl++;
                    continue;
                }
                if (positionValue[p / 10][p % 10] == 1) {
                    co++;
                } else if (positionValue[p / 10][p % 10] == -1) {
                    pl++;
                }
            }
            if (sequence.contains(0) || sequence.contains(9) || sequence.contains(90) || sequence.contains(99)) {
                if (co > pl) {
                    pl--;
                } else if (pl > co) {
                    co--;
                } else {
                    co--;
                    pl--;
                }
            }
            if (co - pl > 0) boardScore += Math.pow(3, Math.abs(co - pl));
            else if (pl - co > 0) boardScore -= Math.pow(3, Math.abs(co - pl));
        }

//        for (int i = 0; i < 100; i++) {
//            if (i == 0 || i == 9 || i == 90 || i == 99) continue;
//            if (positionValue[i / 10][i % 10] == 0) continue;
//
//            ArrayList<Integer> position = positionSequences.get(i);
//            for (int j = 0; j < position.size(); j++) {
//
//            }
//        }

        return boardScore;
    }

    public class CardAndScores {
        private int score;
        private int point;

        public CardAndScores(int score, int point) {
            this.score = score;
            this.point = point;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }
    }

}
