package com.incredible.sequence.Objects;

import java.util.ArrayList;

public class Player {
    private ArrayList<Card> cards;
    private ArrayList<Integer> sequences;
    private Coin coin;
    private int noOfSequences = 0;

    public Player(ArrayList<Card> cards, Coin coin) {
        this.cards = cards;
        this.coin = coin;
        sequences = new ArrayList<>();
        noOfSequences = 0;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public Coin getCoin() {
        return coin;
    }

    public ArrayList<Integer> getSequences() {
        return sequences;
    }

    public void setSequences(ArrayList<Integer> sequences) {
        this.sequences = sequences;
    }

    public int getNoOfSequences() {
        return noOfSequences;
    }

    public void setNoOfSequences(int noOfSequences) {
        this.noOfSequences = noOfSequences;
    }
}
