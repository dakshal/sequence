package com.incredible.sequence.Objects.AI;

import android.util.Log;

import com.google.gson.Gson;
import com.incredible.sequence.Objects.Board;
import com.incredible.sequence.Objects.BoardCard;
import com.incredible.sequence.Objects.Card;
import com.incredible.sequence.Objects.Coin;
import com.incredible.sequence.Objects.Player;
import com.incredible.sequence.Objects.Status;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class AIPlayer {

    public static final int MATCH_WIN_COST = 1;
    public static final int OPPONENT_PANELTY = -10;
    public static final int MATCH_LOSE_COST = -100;
    public static final int DRAW_COST = 0;
    public static final String TAG = "AIPlayer";
    private Board board;
    private HashMap<Integer, ArrayList<Integer>> cardMapping;
    private Status status;
    private Player player;
    private Player opponent;
    private int uptoDepth = 10;
    private BoardCard[][] game;
    ArrayList<CardAndScores> rootChildScores = new ArrayList<>();
    BoardCard[][] newGame;
    Gson gson = new Gson();
    private int optionSize = 10;

    public void setOptionSize(int optionSize) {
        this.optionSize = optionSize;
    }

    public AIPlayer(Board board, int uptoDepth, Player player, Player opponent) {
        this.board = board;
        this.uptoDepth = uptoDepth;
        this.player = player;
        this.opponent = opponent;
        game = board.getGame();
        cardMapping = board.getCardMapping();
        status = new Status();
        Logger.addLogAdapter(new AndroidLogAdapter());

    }

    public void createWeightedMap(BoardCard[][] game, int color) {

        int[][] scoreMap = new int[10][10];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                int count = 0;
                for (int k = 0; k < 5; k++) {
                    int posColor = game[i][j].getCoin().getColor();
                    if (posColor == color || posColor == 0) {

                    }
                }

                if (game[i][j].getCoin().getColor() == color && game[i + 1][j].getCoin().getColor() == color && game[i + 2][j].getCoin().getColor() == color && game[i + 3][j].getCoin().getColor() == color && game[i + 4][j].getCoin().getColor() == color) {
                    scoreMap[i][j] += 1;
                }

                if (game[i][j + 1].getCoin().getColor() == color && game[i][j + 2].getCoin().getColor() == color && game[i][j + 3].getCoin().getColor() == color && game[i][j + 4].getCoin().getColor() == color) {
//                    scoreMap[color] += 1;
                }

                if (game[i + 1][j + 1].getCoin().getColor() == color && game[i + 2][j + 2].getCoin().getColor() == color && game[i + 3][j + 3].getCoin().getColor() == color && game[i + 4][j + 4].getCoin().getColor() == color) {
//                    scoreMap[color] += 1;
                }

                color = game[i][j + 4].getCoin().getColor();

                if (game[i + 1][j + 3].getCoin().getColor() == color && game[i + 2][j + 2].getCoin().getColor() == color && game[i + 3][j + 1].getCoin().getColor() == color && game[i + 4][j].getCoin().getColor() == color) {
//                    scoreMap[color] += 1;
                }
            }
        }
    }

    public AICard nextStep() {
        rootChildScores.clear();
        ArrayList<Card> cards = player.getCards();
        int[][] convertedGame = convertBoardtoInt(game, 1, -1);
        alphaBetaMinimax(Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 1, cards, new ArrayList<Card>(), convertedGame);

        int pos = getBestMove();

        Log.d(TAG, "nextStep: " + pos);
        Status status = new Status();
        Player playerUpdated = status.isGameOver(game, player);
        Log.d(TAG, "nextStep: player:- " + gson.toJson(playerUpdated));
        Log.d(TAG, "nextStep: game:- " + gson.toJson(game));
        Log.d(TAG, "nextStep: newGame:- " + gson.toJson(newGame));
        Log.d(TAG, "nextStep: rootChildScores:- " + gson.toJson(rootChildScores));
        Card playerCard = null;
        for (Card card : player.getCards()) {
            if (cardMapping.get(card.getNumber()).contains(pos)) {
                playerCard = card;
            }
            if (game[pos / 10][pos % 10].getCoin() != null && cardMapping.get(card.getNumber()).contains(-1)) {
                playerCard = card;
            }
        }
        if (playerCard == null) {
            for (Card card : player.getCards()) {
                if (cardMapping.get(card.getNumber()).contains(100)) {
                    playerCard = card;
                }
            }
        }

        if (playerCard == null) {
            AICard aiCard = randomCardSelector(player);
            return aiCard;
        }

        AICard aiCard = new AICard(pos / 10, pos % 10, pos, playerCard);
        return aiCard;
    }

    public AICard randomCardSelector(Player player) {
        Random random = new Random();
        boolean selected = false;
        while (!selected) {
            int cardNumber = random.nextInt(player.getCards().size());
            Card playerCard = player.getCards().get(cardNumber);
            ArrayList<Integer> indexes = cardMapping.get(playerCard.getNumber());
            int select = indexes.get(0);
            if (select == -1) {
                ArrayList<Integer> def_pos = findDefensivePositions(convertBoardtoInt(game, 1, -1), 1);
                selected = true;
                select = def_pos.get(0);
                AICard aiCard = new AICard(select / 10, select % 10, def_pos.get(0), playerCard);
                return aiCard;
            } else if (select == 100) {
                ArrayList<Integer> off_pos = findOffensivePositions(convertBoardtoInt(game, 1, 1), 1);
                selected = true;
                select = off_pos.get(0);
                AICard aiCard = new AICard(select / 10, select % 10, off_pos.get(0), playerCard);
                return aiCard;
            }
            if (game[select / 10][select % 10].getCoin() == null) {
                selected = true;
                AICard aiCard = new AICard(select / 10, select % 10, select, playerCard);
                return aiCard;
            }
            select = indexes.get(1);
            if (game[select / 10][select % 10].getCoin() == null) {
                selected = true;
                AICard aiCard = new AICard(select / 10, select % 10, select, playerCard);
                return aiCard;
            }

        }
        return null;
    }


    private int getBestMove() {
        int max = Integer.MIN_VALUE;
        int best = 0;
        for (int i = 0; i < rootChildScores.size(); i++) {
            if (max < rootChildScores.get(i).getScore()) {
                Log.d(TAG, "getBestMove: position:- " + rootChildScores.get(i).getPoint() + "\t\tscore:- " + rootChildScores.get(i).getScore());
                max = rootChildScores.get(i).getScore();
                best = rootChildScores.get(i).getPoint();
            }
        }
        Log.d(TAG, "getBestMove: ");
        return best;
    }

    public int alphaBetaMinimax(int alpha, int beta, int depth, int turn, ArrayList<Card> playerCards, ArrayList<Card> usedCards, int[][] convertedGame) {

        if (beta <= alpha) {
            if (turn == 1) {
                return Integer.MAX_VALUE;
            } else {
                return Integer.MIN_VALUE;
            }
        }

        int gameStatus = 0;
        //status.isGameOver(convertedGame, player, opponent);

        if (depth == uptoDepth || gameStatus >= 20 || gameStatus == 2 || gameStatus == 12) {
            printTable(convertedGame, "testing convertedGame\t");
            return evaluateBoard(convertedGame, turn);
        }

        ArrayList<Integer> pointsAvailable = getAvailableStates(convertedGame, playerCards, usedCards);

        if (pointsAvailable.isEmpty()) {
            return 0;
        }

        if (depth == 0) {
            rootChildScores.clear();
        }

        int maxValue = Integer.MIN_VALUE, minValue = Integer.MAX_VALUE;

        for (int i = 0; i < pointsAvailable.size(); i++) {
            int point = pointsAvailable.get(i);
            if (point > 0 && point < 99) {
                Card playerCard = null;
                for (int j = 0; j < playerCards.size(); j++) {
                    Log.d(TAG, "alphaBetaMinimax: " + j);
                    if (playerCards.get(j).getNumber() == game[point / 10][point % 10].getCard().getNumber()) {
                        playerCard = playerCards.get(j);
                        break;
                    }
                }

                int currentScore = 0;

                Log.e(TAG, "alphaBetaMinimax: depth:- " + depth);
                if (turn == 1) {
                    point = placeAMove(point, convertedGame, 1);
                    usedCards.add(playerCard);
                    currentScore = alphaBetaMinimax(alpha, beta, depth + 1, 2, playerCards, usedCards, convertedGame);
                    maxValue = Math.max(maxValue, currentScore);

                    //Set alpha
                    alpha = Math.max(currentScore, alpha);

                    if (depth == 0)
                        rootChildScores.add(new CardAndScores(currentScore, point));
                } else if (turn == 2) {
                    placeAMove(point, convertedGame, 2);
                    currentScore = alphaBetaMinimax(alpha, beta, depth + 1, 1, playerCards, usedCards, convertedGame);
                    minValue = Math.min(minValue, currentScore);

                    //Set beta
                    beta = Math.min(currentScore, beta);
                }
//            //reset board

                Log.e(TAG, "alphaBetaMinimax: removing at point:-- " + point + "\tturn:- " + turn);
                convertedGame[point / 10][point % 10] = 0;
                usedCards.remove(playerCard);

                //If a pruning has been done, don't evaluate the rest of the sibling states
                if (currentScore == Integer.MAX_VALUE || currentScore == Integer.MIN_VALUE) break;
            }
        }
        return turn == 1 ? maxValue : minValue;
    }

    public int placeAMove(int point, int[][] state, int turn) {
        if (turn == 1) {     // Computer turn
            if (point == -1) {
                point = findDefensivePositions(state, turn).get(0);
                if (state[point / 10][point % 10] != 0) {
                    state[point / 10][point % 10] = 0;
                }
            } else if (point == 100) {
                point = findOffensivePositions(state, turn).get(0);
                state[point / 10][point % 10] = 1;
            } else {
                state[point / 10][point % 10] = 1;
            }

        } else if (turn == 2) {       // Player turn
            point = findOffensivePositions(state, turn).get(0);
            state[point / 10][point % 10] = -1;
        }
        printTable(state, "placeAMove testing\t");
        Log.e(TAG, "placeAMove: placing at point:-- " + point + "\tturn+" + turn);
        return point;
    }

    void printTable(int[][] positionValue, String tag) {
        Log.d(TAG, tag + ": \t\t----------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            Log.d(TAG, tag + ": \t\t" + gson.toJson(positionValue[i]));
        }
        Log.d(TAG, tag + ": \t\t----------------------------------------------------");
    }

    public ArrayList<Integer> findOffensivePositions(int[][] state, int turn) {
        int positionValue[][] = swapValues(state, turn, 1);

        float playerCount = 0, computerCount = 0, currentScore = 0, cnt = 0, midCnt = 0;
        float threats[][] = new float[10][10];


        printTable(positionValue, "initial findOffensivePositions\t\t");

        int r = 10, x, y;
        int seqlen = 5;
        float nullScore = 0;

        /*
        * check all the positions weights for next move
        */
        for (int i = 0; i < r * r; i++) {
            x = i / r;
            y = i % r;
            if (state[x][y] == 0) {
                playerCount = 0;
                computerCount = 0;
                currentScore = 0;
                cnt = 0;
//                Log.d(TAG, "findOffensivePositions:\tH\t j=" + (((y - seqlen) >= 0) ? (y - seqlen) : 0) + "\tj<" + (((y + seqlen < r) ? (y + seqlen) : r)));
                int m = y - seqlen;
                int n = y + seqlen;
                /*
                * checking horizontal/row sequences
                * */
                for (int j = ((m + 1) >= 0) ? (m + 1) : 0; j < ((n < r) ? (n) : r); j++) {

                    /*
                    * check if it is already a part of sequence
                    * */
                    if (j == y && cnt == -4 && (m) >= 0) {
                        if (positionValue[x][m] == -1) {
                            cnt = 0;
                            playerCount = -1;
                        } else if (x == 0 && m == 0) {
                            cnt = 0;
                            playerCount = -1;
                        } else if (x == 9 && m == 0) {
                            cnt = 0;
                            playerCount = -1;
                        }
                    } else if (j == y && cnt == 4 && (m) >= 0) {
                        if (positionValue[x][m] == 1) {
                            cnt = 0;
                            computerCount = 1;
                        } else if (x == 0 && m == 0) {
                            cnt = 0;
                            computerCount = 1;
                        } else if (x == 9 && m == 0) {
                            cnt = 0;
                            computerCount = 1;
                        }
                    }

                    if (j == y) continue;
                    //   System.out.println("rows:\t"+i+"\t"+j+"\t"+i/r+"\t"+i%r);
                    /*
                    * check for corners
                    * */

                    if ((x * r + j) == 0 || (x * r + j) == 9 || (x * r + j) == 90 || (x * r + j) == 99) {
                        playerCount += (playerCount - 1);
                    }

                    if (positionValue[x][j] == -1) {
                        playerCount += (playerCount + positionValue[x][j]);
                        currentScore += (positionValue[x][j]);
                        if (cnt < 0) {
                            currentScore += (cnt * currentScore);
                        } else {
                            cnt = 0;
                        }
                        cnt--;
                        if (j > y) {
                            midCnt--;
                        }
                    } else if (positionValue[x][j] == 1) {
                        computerCount += (computerCount + positionValue[x][j]);
                        currentScore += (positionValue[x][j]);
                        if (cnt > 0) {
                            currentScore += (cnt * currentScore);
                        } else {
                            cnt = 0;
                        }
                        cnt++;
                        if (j > y) {
                            midCnt++;
                        }
                    } else {
                        currentScore += nullScore;
                        cnt -= cnt / 8;
                    }
                    if (j == n - 1 && midCnt == -4 && (n + 1) < r) {
                        if (positionValue[x][n + 1] == -1) {
                            cnt = 0;
                            playerCount += 15;
                        } else if (x == 0 && n + 1 == 9) {
                            cnt = 0;
                            playerCount += 15;
                        } else if (x == 9 && n + 1 == 9) {
                            cnt = 0;
                            playerCount += 15;
                        }
                    } else if (j == n - 1 && midCnt == 4 && (n + 1) < r) {
                        if (positionValue[x][n + 1] == 1) {
                            cnt = 0;
                            computerCount -= 15;
                        } else if (x == 0 && n + 1 == 9) {
                            cnt = 0;
                            computerCount -= 15;
                        } else if (x == 9 && n + 1 == 9) {
                            cnt = 0;
                            computerCount -= 15;
                        }
                    }
                }
                if (playerCount <= -15) {
                    threats[x][y] += 1000;
                } else if (computerCount >= 15) {
                    threats[x][y] += 1000;
                }
//                } else {
                threats[x][y] += Math.abs(currentScore);
//                }

                playerCount = 0;
                computerCount = 0;
                currentScore = 0;
                cnt = 0;
                midCnt = 0;
                m = x - seqlen;
                n = x + seqlen;

                //                Log.d(TAG, "findOffensivePositions:\tV\t j=" + (((x - seqlen) >= 0) ? (x - seqlen) : 0) + "\tj<" + ((x + seqlen < r) ? (x + seqlen) : r));
                for (int j = ((m + 1) >= 0) ? (m + 1) : 0; j < ((n < r) ? (n) : r); j++) {

                    if (j == y && cnt == -4 && (m) >= 0) {
                        if (positionValue[m][y] == -1) {
                            cnt = 0;
                            playerCount = -1;
                        } else if (m == 0 && y == 0) {
                            cnt = 0;
                            playerCount = -1;
                        } else if (y == 9 && m == 0) {
                            cnt = 0;
                            playerCount = -1;
                        }
                    } else if (j == y && cnt == 4 && (m) >= 0) {
                        if (positionValue[m][y] == 1) {
                            cnt = 0;
                            playerCount = 1;
                        } else if (m == 0 && y == 0) {
                            cnt = 0;
                            playerCount = 1;
                        } else if (m == 0 && y == 9) {
                            cnt = 0;
                            playerCount = 1;
                        }
                    }

                    if (j == y) continue;
                    //   System.out.println("rows:\t"+i+"\t"+j+"\t"+i/r+"\t"+i%r);
                    if ((x * r + j) == 0 || (x * r + j) == 9 || (x * r + j) == 90 || (x * r + j) == 99) {
                        playerCount += (playerCount - 1);
                    }
                    //   System.out.println("cols:\t"+i+"\t"+j+"\t"+i/r+"\t"+i%r);
                    if (positionValue[j][y] == -1) {
                        playerCount += (playerCount + positionValue[j][y]);
                        currentScore += (positionValue[j][y]);
                        if (cnt < 0) {
                            currentScore += (cnt * currentScore);
                        } else {
                            cnt = 0;
                        }
                        cnt--;
                        if (j > x) {
                            midCnt--;
                        }
                    } else if (positionValue[j][y] == 1) {
                        computerCount += (computerCount + positionValue[j][y]);
                        currentScore += (positionValue[j][y]);
                        if (cnt > 0) {
                            currentScore += (cnt * currentScore);
                        } else {
                            cnt = 0;
                        }
                        cnt++;
                        if (j > x) {
                            midCnt++;
                        }
                    } else {
                        currentScore += nullScore;
                        cnt -= cnt / 8;
                    }
                    if (j == n - 1 && midCnt == -4 && (n + 1) < r) {
                        if (positionValue[n + 1][y] == -1) {
                            cnt = 0;
                            playerCount += 15;
                        } else if (n + 1 == 9 && y == 0) {
                            cnt = 0;
                            playerCount += 15;
                        } else if (n + 1 == 9 && y == 9) {
                            cnt = 0;
                            playerCount += 15;
                        }
                    } else if (j == 4 && midCnt == 4 && (n + 1) < r) {
                        if (positionValue[n + 1][y] == 1) {
                            cnt = 0;
                            playerCount -= 15;
                        } else if (n + 1 == 9 && y == 0) {
                            cnt = 0;
                            playerCount -= 15;
                        } else if (n + 1 == 9 && y == 9) {
                            cnt = 0;
                            playerCount -= 15;
                        }
                    }
                }
                if (playerCount <= -15) {
                    threats[x][y] += 1000;
                } else if (computerCount >= 15) {
                    threats[x][y] += 1000;
                }
//                } else {
                threats[x][y] += Math.abs(currentScore);
//                }

                int a, b;

                a = (x > y) ? (y - Math.max(y - seqlen + 1, 0)) : (x - Math.max(x - seqlen + 1, 0));
                b = (x < y) ? (Math.min(y + seqlen, r) - y) : (Math.min(x + seqlen, r) - x);

                playerCount = 0;
                computerCount = 0;
                currentScore = 0;
                cnt = 0;
                midCnt = 0;

//                Log.d(TAG, "findOffensivePositions:\tD\t a=" + a + "\tb=" + b);
                if (Math.abs(a + b) >= seqlen) { // first Diagonal
                    m = (x > y) ? (y - Math.max(y - seqlen, 0)) : (x - Math.max(x - seqlen, 0));
                    n = (x < y) ? (Math.min(y + seqlen, 9) - y) : (Math.min(x + seqlen, 9) - x);
                    //   System.out.println("found A Diag:\t"+a+"\t"+b+"\t\t"+i);
                    for (int j = 0 - a; j < b; j++) {
                        if (j == 0 && cnt == -4 && m == 5) {
                            if (positionValue[x - m][y - m] == -1) {
                                cnt = 0;
                                playerCount = -1;
                            } else if (x - m == 0 && y - m == 0) {
                                cnt = 0;
                                playerCount = -1;
                            }
                        } else if (j == 0 && cnt == 4 && m == 5) {
                            if (positionValue[x - m][y - m] == 1) {
                                cnt = 0;
                                playerCount = 1;
                            } else if (x - m == 0 && y - m == 0) {
                                cnt = 0;
                                playerCount = 1;
                            }
                        }
                        if (j == 0) continue;
                        if ((x * r + j) == 0 || (x * r + j) == 9 || (x * r + j) == 90 || (x * r + j) == 99) {
                            playerCount += (playerCount - 1);
                        }
//                        threats[x][y] += (4 - Math.abs(j)) * positionValue[x + j][y + j];
                        if (positionValue[x + j][y + j] == -1) {
                            playerCount += (playerCount + positionValue[x + j][y + j]);
                            currentScore += (positionValue[x + j][y + j]);
                            if (cnt < 0) {
                                currentScore += (cnt * currentScore);
                            } else {
                                cnt = 0;
                            }
                            cnt--;
                            if (j > 0) {
                                midCnt--;
                            }
                        } else if (positionValue[x + j][y + j] == 1) {
                            computerCount += (computerCount + positionValue[x + j][y + j]);
                            currentScore += (positionValue[x + j][y + j]);
                            if (cnt > 0) {
                                currentScore += (cnt * currentScore);
                            } else {
                                cnt = 0;
                            }
                            cnt++;
                            if (j > 0) {
                                midCnt++;
                            }
                        } else {
                            currentScore += nullScore;
                            cnt -= cnt / 8;
                        }
                        if (j == 4 && midCnt == -4 && n == 5) {
                            if (positionValue[x + n][y + n] == -1) {
                                cnt = 0;
                                playerCount = -1;
                            } else if (x + n == 9 && y + n == 9) {
                                cnt = 0;
                                playerCount = -1;
                            }
                        } else if (j == 4 && midCnt == 4 && n == 5) {
                            if (positionValue[x + n][y + n] == 1) {
                                cnt = 0;
                                playerCount = 1;
                            } else if (x + n == 9 && y + n == 9) {
                                cnt = 0;
                                playerCount = 1;
                            }
                        }
                    }
                    if (playerCount <= -15) {
                        threats[x][y] += 1000;
                    } else if (computerCount >= 15) {
                        threats[x][y] += 1000;
                    }
//                    } else {
                    threats[x][y] += Math.abs(currentScore);
//                    }
                }

                playerCount = 0;
                computerCount = 0;
                currentScore = 0;
                cnt = 0;
                midCnt = 0;
//                if (i == 18 || i == 27 || i == 36 || i == 45 || i == 54 || i == 63 || i == 72 || i == 81) {
//                    Log.d(TAG, "findOffensivePositions: breakpoint");
//                    Log.d(TAG, "threats: \t\t----------------------------------------------------");
//                    for (int l = 0; l < 10; l++) {
//                        String msg = "";
//                        for (int j = 0; j < 10; j++) {
//                            msg += "\t" + ((int) threats[l][j]);
//                        }
//                        Log.d(TAG, "threats: \t\t" + msg);
//                    }
//                    Log.d(TAG, "threats: \t\t----------------------------------------------------");
//                }

                if ((x + y) > seqlen - 1 && (x + y) <= (19 - seqlen)) {    // second diagonal
                    for (int j = 0 - 4; j < 5; j++) {
                        if (j == 0) continue;
                        if ((x * r + j) == 0 || (x * r + j) == 9 || (x * r + j) == 90 || (x * r + j) == 99) {
                            playerCount += (playerCount - 1);
                        }
                        if (((x + j) >= 0 && (y - j) >= 0) && ((x + j) < r && (y - j) < r)) {
                            if (j == 0 && cnt == -4 && ((x - 5) >= 0 && (y + 5) < r)) {
                                if (positionValue[x - 5][y + 5] == -1) {
                                    cnt = 0;
                                    playerCount = -1;
                                } else if (x - 5 == 0 && y + 5 == 9) {
                                    cnt = 0;
                                    playerCount = -1;
                                }
                            } else if (j == 0 && cnt == 4 && ((x - 5) >= 0 && (y + 5) < r)) {
                                if (positionValue[x - 5][y + 5] == 1) {
                                    cnt = 0;
                                    playerCount = 1;
                                } else if (x - 5 == 0 && y + 5 == 9) {
                                    cnt = 0;
                                    playerCount = 1;
                                }
                            }
//                            Log.d(TAG, "findOffensivePositions: x=" + (x + j) + "\ty=" + (y - j) + "\tcount=" + currentScore + "\tcnt=" + cnt);
                            if (positionValue[x + j][y - j] == -1) {
                                playerCount += (playerCount + positionValue[x + j][y - j]);
                                currentScore += (positionValue[x + j][y - j]);
                                if (cnt < 0) {
                                    currentScore += (cnt * currentScore);
                                } else {
                                    cnt = 0;
                                }
                                cnt--;
                                if (j > 0) {
                                    midCnt--;
                                }
                            } else if (positionValue[x + j][y - j] == 1) {
                                computerCount += (computerCount + positionValue[x + j][y - j]);
                                currentScore += (positionValue[x + j][y - j]);
                                if (cnt > 0) {
                                    currentScore += (cnt * currentScore);
                                } else {
                                    cnt = 0;
                                }
                                cnt++;
                                if (j > 0) {
                                    midCnt++;
                                }
                            } else {
                                currentScore += nullScore;
                                cnt -= cnt / 8;
                            }
                            if (j == 4 && cnt == -4 && ((y - 5) >= 0 && (x + 5) < r)) {
                                if (positionValue[x + 5][y - 5] == -1) {
                                    cnt = 0;
                                    playerCount = -1;
                                } else if (x + 5 == 9 && y - 5 == 0) {
                                    cnt = 0;
                                    playerCount = -1;
                                }
                            } else if (j == 4 && cnt == 4 && ((y - 5) >= 0 && (x + 5) < r)) {
                                if (positionValue[x + 5][y - 5] == 1) {
                                    cnt = 0;
                                    playerCount = 1;
                                } else if (x + 5 == 9 && y - 5 == 0) {
                                    cnt = 0;
                                    playerCount = 1;
                                }
                            }
                        }
                    }
                    if (playerCount <= -15) {
                        threats[x][y] += 1000;
                    } else if (computerCount >= 15) {
                        threats[x][y] += 1000;
                    }
//                    } else {
                    threats[x][y] += Math.abs(currentScore);
//                    }
                }
//                if (i == 18 || i == 27 || i == 36 || i == 45 || i == 54 || i == 63 || i == 72 || i == 81) {
//                    Log.d(TAG, "threats: \t\t----------------------------------------------------");
//                    for (int l = 0; l < 10; l++) {
//                        String msg = "";
//                        for (int j = 0; j < 10; j++) {
//                            msg += "\t" + ((int) threats[l][j]);
//                        }
//                        Log.d(TAG, "threats: \t\t" + msg);
//                    }
//                    Log.d(TAG, "threats: \t\t----------------------------------------------------");
//                    Log.d(TAG, "findOffensivePositions: breakpoint");
//                }
            }
        }

        Log.d(TAG, "threats: \t\t----------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            String msg = "";
            for (int j = 0; j < 10; j++) {
                msg += "\t" + ((int) threats[i][j]);
            }
            Log.d(TAG, "threats: \t\t" + msg);
        }
        Log.d(TAG, "threats: \t\t----------------------------------------------------");

        ArrayList<Integer> positions = new ArrayList<>();

        for (int k = 0; k < optionSize; k++) {
            int position = -1;
            float value = -1000;
            for (int i = 0; i < 100; i++) {
                if (threats[i / 10][i % 10] > value && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == 0) {
                    value = threats[i / 10][i % 10];
                    position = i;
                }
            }
            if (positions.contains(position)) {
                continue;
            }
            if (position == -1) {
                break;
            }
            positions.add(position);
            threats[position / 10][position % 10] = Integer.MIN_VALUE;
        }

//        Log.d(TAG, "findOffensivePositions: " + position + "\tval:- " + value);

        return positions;
    }

    private int[][] swapValues(int[][] state, int turn, int offense) {
        int positionValue[][] = new int[10][10];

        int playerCoin = 1;
        int opponentCoin = -1;

        if (turn != 1) {
            playerCoin += opponentCoin;
            opponentCoin = playerCoin - opponentCoin;
            playerCoin = playerCoin - opponentCoin;
        }
        int x, y;
        if (offense == 1 && turn == 1) {
            for (int i = 0; i < player.getCards().size(); i++) {
                ArrayList<Integer> positions = board.getCardMapping().get(player.getCards().get(i).getNumber());
                if (positions.contains(100) || positions.contains(-1)) {
                    continue;
                }
                for (int pos : positions) {
                    positionValue[pos / 10][pos % 10] = 1;
                }
            }
        }

        for (int i = 0; i < 100; i++) {
            x = i / 10;
            y = i % 10;
            if (state[x][y] != 0) {
                if (state[x][y] == playerCoin) {
                    positionValue[x][y] = 1;
                } else if (state[x][y] == opponentCoin) {
                    positionValue[x][y] = -1;
                } else {
                    positionValue[x][y] = offense;
                }
            }
        }

        return positionValue;
    }

    public ArrayList<Integer> findDefensivePositions(int[][] state, int turn) {
        int[][] positionValue = swapValues(state, turn, -1);

        int threats[][] = new int[10][10];
        int r = 10;

        Player opponent, player;
        if (turn != 1) {
            opponent = this.player;
            player = this.opponent;
        } else {
            opponent = this.opponent;
            player = this.player;
        }

        int x, y;
        int seqlen = 5;

        for (int i = 0; i < r * r; i++) {
            x = i / 10;
            y = i % 10;
            if (!opponent.getSequences().contains(i)) {
                for (int j = ((y - seqlen) >= 0) ? (y - seqlen) : 0; j < ((y + seqlen < r) ? (y + seqlen) : r); j++) {
                    //   System.out.println("rows:\t"+i+"\t"+j+"\t"+i/r+"\t"+i%r);
                    threats[x][y] += (seqlen - Math.abs(y - j)) * (positionValue[x][j]);
                }
                for (int j = ((x - seqlen) >= 0) ? (x - seqlen) : 0; j < ((x + seqlen < r) ? (x + seqlen) : r); j++) {
                    //   System.out.println("cols:\t"+i+"\t"+j+"\t"+i/r+"\t"+i%r);
                    threats[x][y] += (seqlen - Math.abs(x - j)) * (positionValue[j][y]);
                }

                int a, b, c, d;

                a = (x > y) ? (Math.min(y, seqlen - 1)) : (Math.min(x, seqlen - 1));
                b = (x < y) ? (Math.min(r - y - 1, seqlen - 1)) : (Math.min(r - x - 1, seqlen - 1));

                c = (x > y) ? (Math.min(r - x, 3)) : (Math.min(r - y, 3));
                d = (x > y) ? (Math.min(r - y, 3)) : (Math.min(r - x, 3));

                if (Math.abs(a + b) >= 5) {
                    //   System.out.println("found A Diag:\t"+a+"\t"+b+"\t\t"+i);
                    for (int j = 0 - a; j <= b; j++) {
                        threats[x][y] += (seqlen - Math.abs(j)) * positionValue[x + j][y + j];
                    }
                }
                if (Math.abs(c + d) >= 5) {
                    //   System.out.println("found A Diag:\t"+c+"\t"+d+"\t\t"+i);
                    // 			for(int j = 0-a; j<=b; j++){
                    // 			    threats[i/r][i%r] = (4-Math.abs(i/r - Math.abs(j)))*pos[i/r+j][i%r+j];
                    // 			}
                }
            }
        }


        ArrayList<Integer> positions = new ArrayList<>();

        for (int k = 0; k < optionSize; k++) {
            int position = -1, value = 1000;
            for (int i = 0; i < r * r; i++) {
                if (threats[i / 10][i % 10] < value && (i != 0 && i != 9 && i != 90 && i != 99) && positionValue[i / 10][i % 10] == -1 && !opponent.getSequences().contains(i) && !player.getSequences().contains(i)) {
                    value = threats[i / 10][i % 10];
                    position = i;
                }
            }
            if (positions.contains(position)) {
                continue;
            }
            if (position == -1) {
                break;
            }
            positions.add(position);
            threats[position / 10][position % 10] = Integer.MAX_VALUE;
        }

        return positions;
    }

    public int[][] convertBoardtoInt(BoardCard[][] state, int turn, int offense) {
        int positionValue[][] = new int[10][10];

        int playerCoin = player.getCoin().getColor();
        int opponentCoin = opponent.getCoin().getColor();

        if (turn != 1) {
            playerCoin += opponentCoin;
            opponentCoin = playerCoin - opponentCoin;
            playerCoin = playerCoin - opponentCoin;
        }
        int x, y;
        if (offense == 1) {
            for (int i = 0; i < player.getCards().size(); i++) {
                ArrayList<Integer> positions = board.getCardMapping().get(player.getCards().get(i).getNumber());
                if (positions.contains(-1) || positions.contains(100)) {
                    continue;
                }
                for (int pos : positions) {
                    positionValue[pos / 10][pos % 10] = 1;
                }
            }
        }

        for (int i = 0; i < 100; i++) {
            x = i / 10;
            y = i % 10;
            if (state[x][y].getCoin() != null) {
                if (state[x][y].getCoin().getColor() == playerCoin) {
                    positionValue[x][y] = 1;
                } else if (state[x][y].getCoin().getColor() == opponentCoin) {
                    positionValue[x][y] = -1;
                } else {
                    positionValue[x][y] = offense;
                }
            }
        }

        return positionValue;
    }

    private ArrayList<Integer> getAvailableStates(int[][] game, ArrayList<Card> playerCards, ArrayList<Card> usedCards) {
        ArrayList<Integer> positions = new ArrayList<>();

        for (Card playerCard : playerCards) {
            ArrayList<Integer> cardPositions = cardMapping.get(playerCard.getNumber());
            if (!cardPositions.contains(-1) && !cardPositions.contains(100) && !usedCards.contains(playerCard)) {
                for (int position : cardPositions) {
                    if (game[position / 10][position % 10] == 0 && !positions.contains(position)) {
                        positions.add(position);
                    }
                }
            } else if (cardPositions.contains(-1) || cardPositions.contains(100)) {
                positions.add(cardPositions.get(0));
            }
        }

        return positions;
    }

    private int evaluateBoard(int[][] positionValue, int turn) {
//        int positionValue[][] = convertBoardtoInt(state, turn, -1);

//        Logger.d(gson.toJson(positionValue));

        int boardScore = 0;
        int playerCount = 0, computerCount = 0, currentScore = 0;

        Log.d(TAG, "evaluateBoard: \t\t---------------------------------------------------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            Log.d(TAG, "evaluateBoard: \t\t" + gson.toJson(positionValue[i]));
            for (int j = 0; j < 10; j++) {
                if (j < 6) {
                    playerCount = 0;
                    computerCount = 0;
                    currentScore = 0;
                    for (int k = 0; k < 5; k++) {
                        switch ((i * 10) + j + k) {
                            case 0:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 9:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 90:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 99:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            default:
                                if (positionValue[i][j + k] == -1) {
                                    playerCount += (playerCount + positionValue[i][j + k]);
                                    currentScore += (currentScore + positionValue[i][j + k]);
                                } else if (positionValue[i][j + k] == 1) {
                                    computerCount += (computerCount + positionValue[i][j + k]);
                                    currentScore += (currentScore + positionValue[i][j + k]);
                                }
                                break;
                        }
                    }
                    if (playerCount == -31) {
                        boardScore += -1000;
                    } else if (computerCount == 31) {
                        boardScore += 1000;
                    } else {
                        boardScore += currentScore;
                    }
                }
                if (i < 6) {
                    playerCount = 0;
                    computerCount = 0;
                    currentScore = 0;
                    for (int k = 0; k < 5; k++) {
                        switch ((i * 10) + j + k) {
                            case 0:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 9:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 90:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 99:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            default:
                                if (positionValue[i + k][j] == -1) {
                                    playerCount += (playerCount + positionValue[i + k][j]);
                                    currentScore += (currentScore + positionValue[i + k][j]);
                                } else if (positionValue[i + k][j] == 1) {
                                    computerCount += (computerCount + positionValue[i + k][j]);
                                    currentScore += (currentScore + positionValue[i + k][j]);
                                }
                                break;
                        }
                    }
                    if (playerCount == -31) {
                        boardScore += -1000;
                    } else if (computerCount == 31) {
                        boardScore += 1000;
                    } else {
                        boardScore += currentScore;
                    }
                }
                if (i < 6 && j < 6) { //diagonals
                    playerCount = 0;
                    computerCount = 0;
                    currentScore = 0;
                    for (int k = 0; k < 5; k++) {
                        switch ((i * 10) + j + k) {
                            case 0:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 9:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 90:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 99:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            default:
                                if (positionValue[i + k][j + k] == -1) {
                                    playerCount += (playerCount + positionValue[i + k][j + k]);
                                    currentScore += (currentScore + positionValue[i + k][j + k]);
                                } else if (positionValue[i + k][j + k] == 1) {
                                    computerCount += (computerCount + positionValue[i + k][j + k]);
                                    currentScore += (currentScore + positionValue[i + k][j + k]);
                                }
                                break;
                        }
                    }
                    if (playerCount == -31) {
                        boardScore += -1000;
                    } else if (computerCount == 31) {
                        boardScore += 1000;
                    } else {
                        boardScore += currentScore;
                    }

                    playerCount = 0;
                    computerCount = 0;
                    currentScore = 0;
                    for (int k = 0; k < 5; k++) {
                        switch ((i * 10) + j + k) {
                            case 0:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 9:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 90:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            case 99:
                                playerCount++;
                                currentScore += (computerCount + ((currentScore >= 0) ? (1) : (-1)));
                                computerCount--;
                                break;
                            default:
                                if (positionValue[i + (4 - k)][j + k] == -1) {
                                    playerCount += (playerCount + positionValue[i + (4 - k)][j + k]);
                                    currentScore += (currentScore + positionValue[i + (4 - k)][j + k]);
                                } else if (positionValue[i + (4 - k)][j + k] == 1) {
                                    computerCount += (computerCount + positionValue[i + (4 - k)][j + k]);
                                    currentScore += (currentScore + positionValue[i + (4 - k)][j + k]);
                                }
                                break;
                        }
                    }
                    if (playerCount == -31) {
                        boardScore += -1000;
                    } else if (computerCount == 31) {
                        boardScore += 1000;
                    } else {
                        boardScore += currentScore;
                    }
                }
            }
        }
        Log.d(TAG, "evaluateBoard: \t\t---------------------------------------------------------------------------------------------");
        return boardScore;
    }

    public class CardAndScores {
        private int score;
        private int point;

        public CardAndScores(int score, int point) {
            this.score = score;
            this.point = point;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }
    }
}
