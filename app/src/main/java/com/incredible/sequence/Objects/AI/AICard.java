package com.incredible.sequence.Objects.AI;

import com.incredible.sequence.Objects.Card;

/**
 * Created by Dakshal on 12/1/2017.
 */

public class AICard {
    private int x, y;
    private int value;
    private Card card;

    public AICard(int x, int y, int value, Card card) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.card = card;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getValue() {
        return value;
    }

    public Card getCard() {
        return card;
    }
}
