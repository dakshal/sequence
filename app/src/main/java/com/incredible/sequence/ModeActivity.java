package com.incredible.sequence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.incredible.sequence.Utils.AbstractBaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ModeActivity extends AbstractBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);
        ButterKnife.bind(this);
        Log.d("TAG", "onCreate: view created");
    }

    @OnClick(R.id.bBasicMode)
    public void basicMode() {
        Bundle b = new Bundle();
        b.putBoolean(AppConstant.IS_BASIC, true);
        Intent i = new Intent(ModeActivity.this, ChooseType.class);
        i.putExtras(b);
        startActivity(i);
    }

    @OnClick(R.id.bAdvMode)
    public void advanceMode() {
        Bundle b = new Bundle();
        b.putBoolean(AppConstant.IS_BASIC, false);
        Intent i = new Intent(ModeActivity.this, ChooseType.class);
        i.putExtras(b);
        startActivity(i);
    }
}
